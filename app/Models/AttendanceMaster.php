<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceMaster extends Model
{
    public $timestamps = false;
	protected $table = 'T_ATTENDANCE_MST';
	protected $primaryKey = 'AttnID';

    protected $fillable = [
        'AttnDt','AttnDataID','AttnType','AttnLatitude','AttnLongitude','AttnImgPath'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }

}
