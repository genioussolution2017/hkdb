<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartMstItemMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_CART_MST_ITEM';
    protected $primaryKey = 'CartItemID';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'CartMstID','CartItemMstID','CartPrice','CartQty','CartItemEntDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }
}
