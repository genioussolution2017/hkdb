<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StruMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_STRU_MST';
    protected $primaryKey = 'StruMstID';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'StruMstParentID','StruMstChildID'
    ];
}
