<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataRouteMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_DATA_ROUTE';
	protected $primaryKey = 'RouteID';

    protected $fillable = [
        'RouteDataID','RouteCd','RouteDate','RouteOn','RouteLatitude','RouteLongitude','RouteDataCustID','RouteSortBy'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
