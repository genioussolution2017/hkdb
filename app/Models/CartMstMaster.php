<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartMstMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_CART_MST';
    protected $primaryKey = 'CartID';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'CartDataID','CartDt','CartStatus','CartRemarks','CartEntDt','CartNetIP'
        ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }
}
