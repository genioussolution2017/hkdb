<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCapacityMaster extends Model
{
	public $timestamps = false;
    protected $table = 'T_PRD_CAP_INFO';
    protected $primaryKey = 'PrdCapID';

    protected $fillable = [
    	'PrdCapDt','PrdCapPoint','PrdCapQty'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
