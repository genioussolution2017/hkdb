<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeScreenMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_HOME_MST';
    protected $primaryKey = 'HomeID';

    protected $fillable = [
		'HomeImgID','HomeItemID','HomeMenuID','HomeURL','HomeValidSts','HomeRemarks','HomeEntDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
