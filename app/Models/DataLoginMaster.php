<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataLoginMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_DATA_LOGIN';

    protected $fillable = [
    	'LoginDataID','LoginOn','LoginNetIP'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
