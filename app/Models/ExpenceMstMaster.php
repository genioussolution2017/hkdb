<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenceMstMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_EXPENCE_MST';
    protected $primaryKey = 'ExpenceID';

    protected $fillable = [
    	'ExpenceDataID','ExpenceOn','ExpenceCommonID','ExpencesValue','ExpenceRemarks','ExpencesImgPath','ExpenceEntDt','ExpencesApproveSts','ExpencesApproveOn','ExpencesApproveByDataID'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
