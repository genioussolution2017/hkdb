<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataTrackMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_DATA_TRACK';

    protected $fillable = [
        'LocDataID','LocOn','LocLatitude','LocLongitude','LocIP'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }

}
