<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourDataMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_DATA_TOUR';
	protected $primaryKey = 'TourID';

    protected $fillable = [
        'TourDataID','TourDtFrom','TourDtTo','TourTitle','TourName','TourLocation','TourRemarks','TourValidSts','TourApproveSts','TourApproveBy','TourEntDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
