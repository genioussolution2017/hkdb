<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemReviewMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_ITEM_REVIEW';
    protected $primaryKey = 'ReviewID';

    protected $fillable = [
    	'ReviewItemID','ReviewDataID','ReviewOn','ReviewText','ReviewImgPath','ReviewStarNo','ReviewValidSts','ReviewNetIP'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
