<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhishListMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_WISHLIST_MST';
    protected $primaryKey = 'WishListID';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'WishDataID','WishItemID','WishEntDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }
}
