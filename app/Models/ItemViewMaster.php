<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemViewMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_ITEM_VIEW';
	protected $primaryKey = 'ViewID';

    protected $fillable = [
        'ViewItemID','ViewDataID','ViewOn'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
