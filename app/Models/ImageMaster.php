<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageMaster extends Model
{
    public $timestamps = false;
	protected $table = 'T_IMAGE_MST';
	protected $primaryKey = 'ImgID';

    protected $fillable = [
        'ImgTagRefID','ImgCommonID','ImgPathTag','ImgPath','ImgValidSts','ImgViewID','ImgEntDt','ImgCngDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }
}