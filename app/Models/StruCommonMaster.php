<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StruCommonMaster extends Model
{
	public $timestamps = false;
    protected $table = 'T_STRU_COMMON_MST';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'StruCommonID','StruParentID','StruChildID'
    ];
}
