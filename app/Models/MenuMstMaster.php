<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuMstMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_MENU_MST';
	protected $primaryKey = 'MenuId';

    protected $fillable = [
        'MenuName','MenuDesc','MenuValidSts','MenuParentId','MenuSortBy','MenuUsrId','MenuEntDt','MenuCngDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}

