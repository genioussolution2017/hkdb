<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoSrcMstMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_INFO_SRC_MST';
	protected $primaryKey = 'InfoSrcId';

    protected $fillable = [
    	'InfoSrcName','InfoSrcUrl','InfoSrcDesc','InfoSrcStatus','InfoSrcCtg','InfoSrcTyp','InfoSrcSortBy','InfoSrcUsrId','InfoSrcEntDt','InfoSrcCngDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
