<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemStarMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_ITEM_STAR';

    protected $fillable = [
        'StarItemID','StartDataID','StarNo'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }
}
