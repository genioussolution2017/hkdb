<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMstMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_ITEM_MST';
	protected $primaryKey = 'ItemID';

    protected $fillable = [
        'ItemCd','ItemName','ItemDesc','ItemValidSts','ItemImgID','ItemSKU','ItemMRP','ItemDiscount','ItemMargin','ItemPrice','ItemMargin1','ItemPrice1','ItemUOMID','ItemGST','ItemUsrID','ItemEntDt','ItemCngDt','ItemOdDmCd','ItemOdSfx','ItemOdDmSz','ItemOdKt','ItemOdDmCol','ItemMainSts'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }
}
