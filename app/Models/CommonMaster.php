<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonMaster extends Model
{
    public $timestamps = false;
	protected $table = 'T_COMMON_MASTER';
	protected $primaryKey = 'MstID';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'MstCd','MstName','MstDesc','MstValidSts','MstFlag','MstImagePath','MstParentID','MstSortBy','MstUsrId','MstEntDt','MstCngDt'
    ];
}




