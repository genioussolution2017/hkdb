<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextMstMaster extends Model
{
	public $timestamps = false;
    protected $table = 'T_TEXT_MST';
    protected $primaryKey = 'TextID';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'TextOrgCommonID','TextCommonID','TextValue','TextValidSts','TextUsrID','TextEntDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
