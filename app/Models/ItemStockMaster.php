<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemStockMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_ITEM_STOCK';

    protected $fillable = [
        'StockDataID','StockItemID','StockQty','StockOn','StockAllotCartID','StockAllotOrderQty'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }
}






