<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemChangeRequestMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_ITEM_CHANGE_REQUEST';
	protected $primaryKey = 'RqstID';

    protected $fillable = [
    	'RqstDataID','RqstItemID','RqstTypeCommonID','RqstRemarks','RqstNetIP','RqstOn','RqstStatus'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
