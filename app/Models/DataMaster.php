<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/*use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;*/

class DataMaster extends Model
{
    //use HasApiTokens, Notifiable;
    protected $table = 'T_DATA_MST';
	protected $primaryKey = 'DataID';
	 /* The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'DataCd','DataName','DataLoginOTP','DataLoginPwd','DataLoginTypeID','DataAreaID','DataLatitude','DataLongitude','DataContactName','DataLoginContactNo','DataEmail','DataAltContactName','DataAltContactEmail','DataAltContactNo','DataAddr1','DataAddr2','DataAddrState','DataAddrCity','DataAddrPinCode','DataTinNo','DataRemarks','DataImgID','DataValidSts','DataUsrID','DataEntDt','DataCngDt'
    ];
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','DataLoginPwd',
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }

    /*protected $maps = [
        'DataID '=>' data_id'
        ,'DataCd '=>' data_cd'
        ,'DataName '=>' data_name'
        ,'DataLoginTypeID '=>' data_login_type'
        ,'DataAreaID '=>' data_area_id'
        ,'DataLatitude '=>' latitide'
        ,'DataLongitude '=>' longitide'
        ,'DataContactName '=>' user_name'
        ,'DataLoginContactNo '=>' user_contact_no'
        ,'DataEmail '=>' user_email'
        ,'DataAltContactName '=>' user_alt_name'
        ,'DataAltContactEmail '=>' user_alt_email'
        ,'DataAltContactNo '=>' user_alt_contact_no'
        ,'DataAddr1 '=>' user_addr1'
        ,'DataAddr2 '=>' user_addr2'
        ,'DataAddrState '=>' user_state'
        ,'DataAddrCity '=>' user_city'
        ,'DataAddrPinCode '=>' user_pincode'
        ,'DataTinNo '=>' user_tinno'
        ,'DataRemarks '=>' user_remarks'
        ,'DataImgID '=>' user_image_id'
        ,'DataValidSts '=>' user_valid_status'
    ];
    protected $visible = [ 'DataCd','DataName','DataLoginOTP','DataLoginPwd','DataLoginTypeID','DataAreaID','DataLatitude','DataLongitude','DataContactName','DataLoginContactNo','DataEmail','DataAltContactName','DataAltContactEmail','DataAltContactNo','DataAddr1','DataAddr2','DataAddrState','DataAddrCity','DataAddrPinCode','DataTinNo','DataRemarks','DataImgID','DataValidSts','DataUsrID','DataEntDt','DataCngDt'];
    protected $appends = [ 'DataCd','DataName','DataLoginOTP','DataLoginPwd','DataLoginTypeID','DataAreaID','DataLatitude','DataLongitude','DataContactName','DataLoginContactNo','DataEmail','DataAltContactName','DataAltContactEmail','DataAltContactNo','DataAddr1','DataAddr2','DataAddrState','DataAddrCity','DataAddrPinCode','DataTinNo','DataRemarks','DataImgID','DataValidSts','DataUsrID','DataEntDt','DataCngDt'];

    public function getDateAttribute() {
        return 'foo';
    }*/
}
