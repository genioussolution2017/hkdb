<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedBackMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_FEEDBACK_MST';
    protected $primaryKey = 'FeedbackID';

    protected $fillable = [
		'FeedbackDataID','FeedbackCommonID','FeedbackText','FeedbackImgPath','FeedbackOn','FeedbackNetIP','FeedbackReplyRatingCommonID','FeedbackReplyDataID','FeedbackReply','FeedbackReplyOn','FeedbackReplayStatus'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
