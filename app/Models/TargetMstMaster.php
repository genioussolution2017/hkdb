<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TargetMstMaster extends Model
{
    public $timestamps = false;
    protected $table = 'T_TARGET_MST';
	protected $primaryKey = 'TargetID';

    protected $fillable = [        
    	'TargetDtFrom','TargetDtTo','TargetDataID','TargetQty','TargetValue','TargetValidSts','TargetUsrID','TargetEntDt','TargetCngDt'
    ];

    public function setUpdatedAt($value)
    {
      return NULL;
    }

    public function setCreatedAt($value)
    {
      return NULL;
    }
}
