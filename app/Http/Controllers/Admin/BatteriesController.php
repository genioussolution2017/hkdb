<?php

namespace App\Http\Controllers\Admin;
use DB;
use Alert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Battery;
use App\Models\Station;
use Yajra\DataTables\DataTables;

class BatteriesController extends Controller
{
	public function index()
	{
		$stations = Station::where("status","1")->get();
		return view('admin.batteries')->with(["stations"=>$stations]);
	}

	public function getBatteries(Request $request)
	{
		$qry = Battery::select('batteries.*','stations.station_name','stations.station_id')
				->leftJoin('stations','batteries.available_at_station','stations.station_id')
				->orderBy('battery_id', 'desc')->get();

		$batteries = DataTables::of($qry)
			->addColumn('action', function ($row) {
				$action = "<div style='display: inline-flex;'>
                                &nbsp;<button class='btn btn-info btn-sm' onclick='editBattery(this)'><i class='fa fa-edit'></i></button>
                                &nbsp;<button class='btn btn-danger btn-sm' onclick='deleteBattery(this)'><i class='fa fa-trash'></i></button>
                                &nbsp;<button class='btn btn-warning btn-sm' onclick='statusBattery(this)'><i id='exStatus' class='".($row->status == "0" ? "fa fa-toggle-on" : "fa fa-toggle-off" )."'></i></button>
                            </div>";
				return $action;
			})
			->addIndexColumn()
			->make(TRUE);
		return $batteries;
	}

	public function create(Request $request)
	{
		// Validate form input data
		$request->validate(
			[
				'power_info' => 'required',
				'available_at_station' => 'required'
			]
		);
		// Save content type
		$batteries_data = $request->all();
		$batteries_data["unique_id"] = time();
		$batteries_data["status"] = 1;
		$save = Battery::create($batteries_data);

		if ($save)
		{
			Alert::success('Battery Insert Success')->persistent('Close');
		}
		else{
			Alert::errror('Somthing went wrong.!!!')->persistent('Close');	
		}
		return redirect('admin/batteries');
	}

	public function update(Request $request)
	{	
		$battery_id = $request->input('battery_id');
		$data = $request->except('battery_id','_token');
		$battery = Battery::find($battery_id);
		foreach ($data as $k => $v)
		{
			if ( ! empty($v) || $k=="status")
			{
				echo "string";
				$battery->{$k} = $v;
			}
		}
		$update = $battery->save();
		if ($update)
		{
			Alert::success('Battery Update Success')->persistent('Close');
		}
		else{
			Alert::errror('Somthing went wrong.!!!')->persistent('Close');
		}
		return redirect('admin/batteries');
	}

	public function delete(Request $request)
	{
		// Validate form input data
		$request->validate(
			[
				'battery_id' => 'required'
			]
		);
		
		$battery_id = $request->input('battery_id');
		$delete = Battery::destroy($battery_id);
		if ($delete)
		{
			return response()->json(["status"=>true,"message"=>'Battery Delete Success']);
		}
		else{
			return response()->json(["status"=>false,"message"=>'Somthing went wrong.!!!']);
		}
	}
}
