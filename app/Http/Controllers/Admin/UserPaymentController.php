<?php

namespace App\Http\Controllers\Admin;
use DB;
use Alert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Yajra\DataTables\DataTables;

class UserPaymentController extends Controller
{
	public function index()
	{		
		return view('admin.transactions');
	}

	public function getTransactions(Request $request)
	{
		$qry = Transaction::select('transaction_id','amount','payment_type','name','email','mobile_no' )
				->leftJoin('users','transactions.user_id','users.id')
				->orderBy('transactions.id', 'desc')->get();
		$batteries = DataTables::of($qry)			
			->addIndexColumn()
			->make(TRUE);
		return $batteries;
	}
}
