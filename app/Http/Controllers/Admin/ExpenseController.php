<?php

namespace App\Http\Controllers\Admin;

use App\ExpenceMstMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CommonMaster;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Alert;
use App\Models\UploadImage;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenseType = CommonMaster::where("MstFlagID","83")->where("MstValidSts","Y")->orderBy('MstSortBy')->get();
        return view('admin.expenses')->with(["expenseType"=>$expenseType]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_data = $request->all();

        $validator = Validator::make($request->all(), [
            // 'ExpenceDataID' => 'required',
            'ExpenceOn' => 'required',
            'ExpenceCommonID' => 'required',
            'ExpencesValue' => 'required',
            'ExpenceRemarks' => 'required',
            'ExpencesImgPath' => 'required'
        ]);

        if ($validator->fails()) {
            $SignupList['success'] =  FALSE;
            $SignupList['message'] =  $validator->messages()->first();
            return response()->json($SignupList);
        }

        if ($request->has('ExpencesImgPath')) {
            $image = $request->file('ExpencesImgPath');
            $name = str_slug($request->input('name')).'_'.time();
            $folder = public_path('uploads/images/');
            $expenseImage = $folder . $name. '.' . $image->getClientOriginalExtension();
            uploadOne($image, $folder, 'public', $name);
            $request_data['ExpencesImgPath'] = $expenseImage;
        }

        $expense = ExpenceMstMaster::create($request_data);

        if ($expense)
        {
            Alert::success(config('constants.insert_success'))->persistent('Close');
        }
        else{
            Alert::errror('Somthing went wrong.!!!')->persistent('Close');
        }
        return redirect('admin/expenses');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request_data = $request->all();

        /*$validator = Validator::make($request->all(), [
            // 'ExpenceDataID' => 'required',
            'ExpenceOn' => 'required',
            'ExpenceCommonID' => 'required',
            'ExpencesValue' => 'required',
            'ExpenceRemarks' => 'required',
            'ExpencesImgPath' => 'required'
        ]);

        if ($validator->fails()) {
            $SignupList['success'] =  FALSE;
            $SignupList['message'] =  $validator->messages()->first();
            return response()->json($SignupList);
        }*/

        if ($request->has('ExpencesImgPath')) {
            $image = $request->file('ExpencesImgPath');
            $name = str_slug($request->input('name')).'_'.time();
            $folder = public_path('uploads/images/');
            $expenseImage = $folder . $name. '.' . $image->getClientOriginalExtension();
            uploadOne($image, $folder, 'public', $name);
            $request_data['ExpencesImgPath'] = $expenseImage;
        }

        $expense = ExpenceMstMaster::where('ExpenceID', $request_data['ExpenceID'])->update($request_data);

        if ($expense)
        {
            if($request->ajax())
            {
                return response()->json(["status"=>true,"message"=>'Update Success']);
            }
            Alert::success('Record Successfully updated.')->persistent('Close');
        }
        else{
            if($request->ajax())
            {
                return response()->json(["status"=>false,"message"=>'Somthing went wrong.!!!']);
            }
            Alert::errror('Something went wrong.!!!')->persistent('Close');
        }
        return redirect('admin/expenses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate(
            [
                'ExpenceID' => 'required'
            ]
        );
        $ExpenceID = $request->input('ExpenceID');
        $delete = ExpenceMstMaster::destroy($ExpenceID);
        if ($delete)
        {
            return response()->json(["status"=>true,"message"=>'Delete Success']);
        }
        return response()->json(["status"=>false,"message"=>'Somthing went wrong.!!!']);
    }

    public function getExpenses(Request $request)
    {
        $qry = DB::table('T_EXPENCE_MST')->select('*')->orderBy('ExpenceDataID', 'desc')->get();
        $expenses = DataTables::of($qry)
            ->addColumn('action', function($row){
                $action = "<div style='display: inline-flex;'>
                                &nbsp;<button class='btn btn-info btn-sm' onclick='editExpense(this)'><i class='fa fa-edit'></i></button>
                                &nbsp;<button class='btn btn-danger btn-sm' onclick='deleteExpense(this)'><i class='fa fa-trash'></i></button>
                                &nbsp;<button class='btn btn-warning btn-sm' onclick='statusExpense(this)'><i id='exStatus' class='".($row->ExpencesApproveSts == "Y" ? "fa fa-toggle-on" : "fa fa-toggle-off" )."'></i></button>
                            </div>";
                return $action;
            })->addIndexColumn()->make(TRUE);
        return $expenses;
    }
}
