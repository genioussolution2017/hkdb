<?php

namespace App\Http\Controllers\Admin;
use DB;
use Alert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserWallet;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
	public function index()
	{
		$users = User::all();
		return view('admin.users');
	}

	public function getUsers(Request $request)
	{
		$qry = User::select('users.id','users.name','users.email','users.mobile_no','users.is_block',DB::Raw('IFNULL(total_balance,0) as total_balance') )
				->leftJoin('user_wallets','users.id','user_wallets.user_id')
				->orderBy('users.id', 'desc')->get();
		$users = DataTables::of($qry)
			->addColumn('action', function ($row) {
				$action = "<div style='display: inline-flex;'>
								&nbsp;<button class='btn btn-info btn-sm' onclick='editUser(this)'><i class='fa fa-edit'></i></button>
								&nbsp;<button class='btn btn-warning btn-sm' onclick='statusUser(this)'><i id='stStatus' class='".($row->is_block == "0" ? "fa fa-lock" : "fa fa-unlock" )."'></i></button>
                                
                            </div>";
				return $action;
			})
			->addIndexColumn()
			->make(TRUE);
		return $users;
	}

	public function create(Request $request)
	{
		// Validate form input data
		$request->validate(
			[
				'name' => 'required',
				'mobile_no' => 'required',
				'password' => 'required',
				'email' => 'required'
			]
		);
		// Save content type
		$user_data = $request->all();		
		$user_data['is_block'] = 1;
		$user_data['password'] = Hash::make($user_data['password']);
		/*print_r($user_data);
		die();*/
		$save = User::create($user_data);

		if ($save)
		{
			Alert::success('User Create Success')->persistent('Close');
		}
		else{
			Alert::errror('Somthing went wrong.!!!')->persistent('Close');
		}
		return redirect('admin/users');
	}

	public function update(Request $request)
	{	
		$id = $request->input('id');
		$data = $request->except('id','_token');
		$user = User::find($id);
		foreach ($data as $k => $v)
		{
			if ( ! empty($v) || $k=="is_block")
			{
				$user->{$k} = $v;
			}
		}
		$update = $user->save();
		if ($update)
		{
			Alert::success('User Update Success')->persistent('Close');
		}
		else{
			Alert::errror('Somthing went wrong.!!!')->persistent('Close');
		}
		return redirect('admin/users');
	}
}
