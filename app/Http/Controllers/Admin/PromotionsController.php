<?php

namespace App\Http\Controllers\Admin;
use DB;
use Alert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promotion;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class PromotionsController extends Controller
{
	public function index()
	{
		return view('admin.promotions');
	}

	public function getPromotions(Request $request)
	{
		$qry = Promotion::all();
		$promation = DataTables::of($qry)
			->addColumn('action', function ($row) {
				$action = "<div style='display: inline-flex;'>
                                &nbsp;<button class='btn btn-info btn-sm' onclick='editStation(this)'><i class='fa fa-edit'></i></button>
                                &nbsp;<button class='btn btn-danger btn-sm' onclick='deleteStation(this)'><i class='fa fa-trash'></i></button>
                                &nbsp;<button class='btn btn-warning btn-sm' onclick='statusStation(this)'><i id='stStatus' class='".($row->status == "0" ? "fa fa-toggle-on" : "fa fa-toggle-off" )."'></i></button>
                            </div>";
				return $action;
			})
			->addIndexColumn()
			->make(TRUE);
		return $promation;
	}

	public function create(Request $request)
	{
		// Validate form input data
		$request->validate(
			[
				'promocode' => 'required',
				'discount' => 'required',
				'start_date' => 'required',
				'end_date' => 'required'
			]
		);
		// Save content type
		$promotion_data = $request->all();
		$promotion_data['start_date'] = \DateTime::createFromFormat('m-d-Y H:i', $request->input('start_date'))->format('Y-m-d H:i:s');
		$promotion_data['end_date'] = \DateTime::createFromFormat('m-d-Y H:i', $request->input('end_date'))->format('Y-m-d H:i:s');
		$save = Promotion::create($promotion_data);

		if ($save)
		{
			Alert::success('Promocode Insert Success')->persistent('Close');
		}
		else{
			Alert::errror('Somthing went wrong.!!!')->persistent('Close');
		}
		return redirect('admin/promotions');
	}

	public function update(Request $request)
	{
		$promotion_id = $request->input('promotion_id');
		$data = $request->except('promotion_id','_token');
		$promotion = Promotion::find($promotion_id);
		foreach ($data as $k => $v)
		{
			if ( ! empty($v) || $k=="status")
			{
				if($k=="start_date" || $k=="end_date" ){
					$myDateTime = \DateTime::createFromFormat('m-d-Y H:i', $request->input($k));
					$promotion->{$k} = $myDateTime->format('Y-m-d H:i:s');
					
				}else{					
					$promotion->{$k} = $v;
				}
			}

		}
		$update = $promotion->save();
		if ($update)
		{
			Alert::success('Promocode Update Success')->persistent('Close');
		}
		else{
			Alert::errror('Somthing went wrong.!!!')->persistent('Close');
		}
		return redirect('admin/promotions');
	}

	public function delete(Request $request)
	{
		$request->validate(
			[
				'promotion_id' => 'required'
			]
		);		
		$promotion_id = $request->input('promotion_id');
		$delete = Promotion::destroy($promotion_id);
		if ($delete)
		{
			return response()->json(["status"=>true,"message"=>'Promocode Delete Success']);
		}
		else{
			return response()->json(["status"=>false,"message"=>'Somthing went wrong.!!!']);
		}
	}
}
