<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CommonMaster;
/*use App\Models\Promotion;
use App\Models\Battery;
use App\Models\StationTransction;
use App\User;
use Yajra\DataTables\DataTables;*/

class DashboardController extends Controller
{
	public function index()
	{
		/*with(["stations"=>$stations,"promotions"=>$promotion,"batteries"=>$battery,"users"=>$user]);*/
		/*$loginType = CommonMaster::where("MstFlagID","61")->where("MstValidSts","Y")->orderBy('MstSortBy')->get();
		$userArea = CommonMaster::where("MstFlagID","62")->where("MstValidSts","Y")->orderBy('MstSortBy')->get();
		return view('admin.users')->with(["loginType"=>$loginType,"userArea"=>$userArea]);*/
		return view('admin.dashboard');
    }

    public function getStationTranscation()
	{
		$qry = StationTransction::select('station_transctions.id','stations.station_name','station_transctions.amount')
				->leftJoin('stations','stations.station_id','station_transctions.station_id')
				->orderBy('station_transctions.id', 'desc')->get();
		$StationTranscation = DataTables::of($qry)->make(TRUE);
		return $StationTranscation;
    }
}
