<?php
namespace App\Http\Controllers\Admin;
use DB;
use Alert;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\DataMaster;
use App\Models\CommonMaster;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;
use App\Models\UploadImage;
use App\Models\ImageMaster;

class DataMstController extends Controller
{
    use UploadImage;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loginType = CommonMaster::where("MstFlagID","61")->where("MstValidSts","Y")->orderBy('MstSortBy')->get();
        $userDataArea = CommonMaster::where("MstFlagID","62")->where("MstValidSts","Y")->orderBy('MstSortBy')->get();
        $userDataTitle = CommonMaster::where("MstFlagID","115")->where("MstValidSts","Y")->orderBy('MstSortBy')->get();
        return view('admin.datausers')->with(["loginType"=>$loginType,"userDataArea"=>$userDataArea,"userDataTitle"=>$userDataTitle]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_data = $request->all();
        print_r($user_data["DataProfileImage"]);
        if ($request->has('DataProfileImage')) {
            // Get image file
            $image = $request->file('DataProfileImage');

            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            
            // Define folder path
            $folder = public_path('uploads/images/');

            // Make a file path where image will be stored [ folder path + file name + file extension]
            $profileImage = $folder . $name. '.' . $image->getClientOriginalExtension();

//dd($image, $folder, 'public', $name);
        
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            
            // Set user profile image path in database to filePath
            //$user->user_image = $profileImage;
        }



        $validator = Validator::make($request->all(), [ 
            'DataLoginTypeID' => ['required', 'string', 'max:255'],
            'DataContactTitle' => ['required', 'string', 'max:255'],
            'DataCd' => ['required', 'string', 'max:255'],
            'DataName' => ['required', 'string', 'max:255'],
            'DataContactName' => ['required', 'string', 'max:255'],
            'DataLoginContactNo' => 'required|min:10|numeric',
            'DataEmail' => ['required', 'string', 'email'],
            'DataAddr1' => ['required', 'string', 'max:255'],
            'DataAddr2' => ['required', 'string', 'max:255'],
            'DataAddrState' => ['required', 'string', 'max:255'],
            'DataAddrCity' => ['required', 'string', 'max:255'],
            'DataAddrPinCode' => ['required', 'string', 'max:255'],
        ]);
        if ($validator->fails()) {
            $SignupList['success'] =  FALSE;
            $SignupList['message'] =  $validator->messages()->first();
            return response()->json($SignupList);
        }
        $DataLoginContactNo = $request->input("DataLoginContactNo");
        $DataEmail = $request->input("DataEmail");
        $results = DataMaster::where('DataLoginContactNo', '=',$DataLoginContactNo)
                    ->orWhere('DataEmail', '=',$DataEmail)
                    ->get();
        if(count($results) > 0 && !empty($results)){
            return response()->json(['success' => TRUE, 'message' => 'This mobile no:- '.$DataLoginContactNo.' or Email Id :- '.$DataEmail.'  already exists.!']);
        }    
        $data_user_id = "";

        $user = User::where('email', '=' , $DataEmail)->first();
        if ($user) {
            $error['success'] =  FALSE;
            $error['message'] =  'Email Id :- '.$DataEmail.' already exists.!'; 
            return response()->json($error, 401); 
        }

        $input = $request->all(); 
        $input['name'] = $input['DataContactName']; 
        $input['email'] = $input['DataEmail']; 
        $input['password'] = bcrypt("123456");
        $user = User::create($input); 
        $data_user_id = DB::table('users')->latest('id')->first();
        $data_user_id = $data_user_id->id;

        $profileImage="";
        /*if ($request->has('DataProfileImage')) {
            // Get image file
            print_r($request->file('DataProfileImage'));
            $image = $request->file('DataProfileImage');
            print_r($image);
            die();
            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $profileImage = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            //$user->user_image = $profileImage;
        }*/
        $desktopImage="";
        /*if ($request->has('DataDesktopImage')) {
            // Get image file
            $image = $request->file('DataDesktopImage');
            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $desktopImage = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            //$user->user_image = $desktopImage;
        }*/
        

        
        // DB::enableQueryLog();
        // Save content type
        $user_data = new DataMaster();
        $user_data["DataLoginPwd"] = Hash::make("123456");
        /*$user_data["DataLoginTypeID"] = $request->input('login_type_id');
        $user_data["DataAreaID"] = $request->input('login_area_id');*/
        $user_data["DataCd"] = $request->input('DataCd');
        $user_data["DataName"] = $request->input('DataName');
        $user_data["DataLoginTypeID"] = $request->input('DataLoginTypeID');
        $user_data["DataContactTitle"] = $request->input('DataContactTitle');
        $user_data["DataContactTitleCommonID"] = $request->input('DataContactTitleCommonID');
        $user_data["DataContactName"] = $request->input('DataContactName');
        $user_data["DataLoginContactNo"] = $request->input('DataLoginContactNo');
        $user_data["DataEmail"] = $request->input('DataEmail');
        $user_data["DataAddr1"] = $request->input('DataAddr1');
        $user_data["DataAddr2"] = $request->input('DataAddr2');
        $user_data["DataAddrState"] = $request->input('DataAddrState');
        $user_data["DataAddrCity"] = $request->input('DataAddrCity');
        $user_data["DataAddrPinCode"] = $request->input('DataAddrPinCode');
        $user_data["DataTinNo"] = $request->input('DataTinNo');
        $user_data["DataRemarks"] = $request->input('DataRemarks');
        $user_data["DataAltContactTitleCommonID"] = $request->input('DataAltContactTitleCommonID');
        $user_data["DataAltContactTitle"] = $request->input('DataAltContactTitle');
        $user_data["DataAltContactName"] = $request->input('DataAltContactName');
        $user_data["DataAltContactEmail"] = $request->input('DataAltContactEmail');
        $user_data["DataAltContactNo"] = $request->input('DataAltContactNo');
        $user_data["DataValidSts"] = 'N';
        $user_data["DataEntDt"] = date('Y-m-d H:i:s');
        $user_data["DataUserId"] = $data_user_id;
        $user_data["DataNetIP"] = null;
        $user_data["DataDeviceSecurityKey"] = null;
        $user_data["DataDeviceToken"] = null;
        // var_dump($user_data);
        // print_r($user_data);
        // die();

        $save = $user_data->save();
        $DataID = DB::table('T_DATA_MST')->latest('DataID')->first();
        $DataID = $DataID->DataID;
        /*INSERT IMAGE AGAINS OF USER*/
            $usr_img = new ImageMaster();
            $MstCd = DB::table('T_COMMON_MASTER')->select('MstID','MstCd')->where('MstID', '=', '78')->first();
            $usr_img['ImgTagRefID']=$DataID;
            $usr_img['ImgCommonID']=$MstCd->MstID;
            $usr_img['ImgPathTag']=$MstCd->MstCd;
            $usr_img['ImgPath']='public'.$profileImage;
            $usr_img['ImgValidSts']='Y';
            $usr_img['ImgViewID']='71';
            $usr_img['ImgEntDt']=date('Y-m-d H:i:s');
            $usrImgSave = $usr_img->save();
            $ImgId = DB::table('T_IMAGE_MST')->latest('ImgID')->first();
        /*INSERT IMAGE AGAINS OF USER END*/
        /*UPDATE USER DATA MASTER */
            DB::table('T_DATA_MST')
            ->where("DataID", '=',  $DataID)
            ->update(['DataImgID'=> $ImgId->ImgID]);
        /*UPDATE USER DATA MASTER END*/

        if ($save)
        {
            Alert::success('Record Successfully inserted.')->persistent('Close');
        }
        else{
            Alert::errror('Somthing went wrong.!!!')->persistent('Close');
        }
        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_data = $request->all(); 
        // print_r($user_data);
        $filePath="";
        if(!empty($request->file('DataProfileImage'))){
            if ($request->has('DataProfileImage')) {
                // Get image file
                $image = $request->file('DataProfileImage');
                // Make a image name based on user name and current timestamp
                $name = str_slug($request->input('name')).'_'.time();
                // Define folder path
                $directoryName = '/uploads/images/';
                //Check if the directory already exists.
                if(!is_dir(public_path().$directoryName)){
                    //Directory does not exist, so lets create it.
                    mkdir(public_path().$directoryName, 0777, true);
                }
                // echo public_path().$directoryName. $name. '.' . $image->getClientOriginalExtension();
                // Make a file path where image will be stored [ folder path + file name + file extension]
                $filePath = 'public'.$directoryName . $name. '.' . $image->getClientOriginalExtension();
                // Upload image
                $this->uploadOne($image, $directoryName, 'public', $name);
                // Set user profile image path in database to filePath
                //$user->user_image = $filePath;
            }               
        }
        echo $filePath;
        die();
        $profileImage="";
        if ($request->has('DataProfileImage')) {
            // Get image file
            // print_r($request->file('DataProfileImage'));
            $image = $request->file('DataProfileImage');
            /*print_r($image);
            die();*/
            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $profileImage = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            echo $name;
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            //$user->user_image = $profileImage;
        }
        echo $profileImage;
        die();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUsers(Request $request)
    {
        $usrQry = DB::table('T_DATA_MST')->select('orgTyp.MstName as org_name','DataID as data_id','DataCd as company_cd','DataName as company_name','DataLoginTypeID as login_type_id','loginTyp.MstName as login_type',DB::raw("isnull(area.MstName,'-') as area_type"),DB::raw("concat(usrTitle.MstName,' ',DataContactName) as user_name"),'DataLoginContactNo as contact_no','DataEmail as email',DB::raw("concat(DataAddr1,' ',DataAddr2,' ',DataAddrState,' ',DataAddrCity,' ',DataAddrPinCode) as address"),DB::raw(" isnull(DataTinNo,'-') as t_no"),'DataRemarks as remarks','DataValidSts as valid_status',DB::raw('CASE WHEN DataImgID is null THEN CONCAT(imgp.MstName,imgp.MstDesc) ELSE CONCAT(imgp.MstName,ImgPath) END AS image_path'),'DataContactTitleCommonID as data_title','DataAddr1','DataAddr2','DataAddrState','DataAddrCity','DataAddrPinCode','DataAltContactTitle as alt_title','DataAltContactTitleCommonID as data_alt_title','DataAltContactName as data_alt_name','DataAltContactEmail as data_alt_email','DataAltContactNo as data_alt_contactno','DataRemarks as data_remarks')
            ->leftJoin('T_COMMON_MASTER as loginTyp', 'DataLoginTypeID', '=', 'loginTyp.MstID')
            ->leftJoin('T_COMMON_MASTER as area', 'DataAreaID', '=', 'area.MstID')
            ->leftJoin('T_COMMON_MASTER as orgTyp', 'DataOrgCommonID', '=', 'orgTyp.MstID')
            ->leftJoin('T_COMMON_MASTER as usrTitle', 'DataContactTitleCommonID', '=', 'usrTitle.MstID')
            ->leftjoin('T_COMMON_MASTER as imgp', function($join){$join->where('imgp.MstID','77');})
            ->leftjoin('T_IMAGE_MST', 'DataImgID','=','ImgID')
            ->orderBy('DataID', 'desc')->get();
        $users = DataTables::of($usrQry)
            ->addColumn('action', function($row){
                $action = "<div style='display: inline-flex;'>
                                &nbsp;<button class='btn btn-info btn-sm' onclick='editDataUser(this)'><i class='fa fa-edit'></i></button>
                                &nbsp;<button class='btn btn-danger btn-sm' onclick='deleteDataUser(this)'><i class='fa fa-trash'></i></button>
                                &nbsp;<button class='btn btn-warning btn-sm' onclick='statusDataUser(this)'><i id='exStatus' class='".($row->valid_status == "N" ? "fa fa-toggle-on" : "fa fa-toggle-off" )."'></i></button>
                            </div>";
                return $action;
            })->addIndexColumn()->make(TRUE);
        return $users;
    }
}
