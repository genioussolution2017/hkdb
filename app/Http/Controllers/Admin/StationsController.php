<?php

namespace App\Http\Controllers\Admin;
use DB;
use Alert;
use QrCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Station;
use Yajra\DataTables\DataTables;


class StationsController extends Controller
{
	public function index()
	{
		$stations = Station::all();
		return view('admin.stations');
	}

	public function getStation(Request $request)
	{
		$qry = Station::orderBy('station_id', 'desc')->get();

		$tips = DataTables::of($qry)
			->addColumn('action', function ($row) {
				$action = "<div style='display: inline-flex;'>
                                &nbsp;<button class='btn btn-info btn-sm' onclick='editStation(this)'><i class='fa fa-edit'></i></button>
                                &nbsp;<button class='btn btn-danger btn-sm' onclick='deleteStation(this)'><i class='fa fa-trash'></i></button>
                                &nbsp;<button class='btn btn-warning btn-sm' onclick='statusStation(this)'><i id='stStatus' class='".($row->status == "0" ? "fa fa-toggle-on" : "fa fa-toggle-off" )."'></i></button>
                            </div>";
				return $action;
			})
			->addIndexColumn()
			->make(TRUE);

		return $tips;
	}

	public function create(Request $request)
	{
		// Validate form input data
		$request->validate(
			[
				'station_name' => 'required',
				'station_addr' => 'required',
				'station_unique_id' => 'required',
				'latitude' => 'required',
				'longitude' => 'required'
			]
		);
		// Save content type
		$station_data = $request->all();		
		$station_data['status'] = 1;
		$save = Station::create($station_data);

		if ($save)
		{
			$path = ('uploads/'.time().'.png');
			QrCode::size(1000)->format('png')
			        ->generate('{"station_id":"'.$save->station_id.'"}', public_path($path));
			$save->qr_code = asset($path);
			$save->save();
			Alert::success('Station Insert Success')->persistent('Close');
		}
		else{
			Alert::errror('Somthing went wrong.!!!')->persistent('Close');
		}
		return redirect('admin/stations');
	}

	public function update(Request $request)
	{
		$station_id = $request->input('station_id');
		$data = $request->except('station_id','_token');
		$station = Station::find($station_id);
		foreach ($data as $k => $v)
		{
			if ( ! empty($v) || $k=="status")
			{
				$station->{$k} = $v;
			}
		}
		$update = $station->save();
		if ($update)
		{
			Alert::success('Station Update Success')->persistent('Close');
		}
		else{
			Alert::errror('Somthing went wrong.!!!')->persistent('Close');
		}
		return redirect('admin/stations');
	}

	public function delete(Request $request)
	{
		$request->validate(
			[
				'station_id' => 'required'
			]
		);		
		$station_id = $request->input('station_id');
		$delete = Station::destroy($station_id);
		if ($delete)
		{
			return response()->json(["status"=>true,"message"=>'Station Delete Success']);
		}
		else{
			return response()->json(["status"=>false,"message"=>'Somthing went wrong.!!!']);
		}
	}
}
