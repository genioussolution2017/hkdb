<?php

namespace App\Http\Controllers\Admin;
use DB;
use Alert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BatteryRentHistory;
use Yajra\DataTables\DataTables;

class BatteryRentController extends Controller
{
	public function index()
	{		
		return view('admin.batteryrent');
	}

	public function getRentbattery(Request $request)
	{
		$qry = BatteryRentHistory::select('power_info','unique_id','battery_desc','station_name','latitude','longitude','start_time','end_time', DB::Raw('CASE WHEN ISNULL(end_time) THEN "On-rent" ELSE "Return" END as rstatus'),'name','email','mobile_no' )
				->leftJoin('batteries','battery_rent_histories.battery_id','batteries.battery_id')
				->leftJoin('stations','battery_rent_histories.station_id','stations.station_id')
				->leftJoin('users','battery_rent_histories.user_id','users.id')
				->orderBy('battery_rent_histories.id', 'desc')->get();
		$batteries = DataTables::of($qry)
			->addColumn('action', function ($row) {
				$action = "<div style='display: inline-flex;'>
                                &nbsp;<button class='btn btn-info btn-sm' onclick='editBattery(this)'><i class='fa fa-edit'></i></button>
                                &nbsp;<button class='btn btn-danger btn-sm' onclick='deleteBattery(this)'><i class='fa fa-trash'></i></button>
                                &nbsp;<button class='btn btn-warning btn-sm' onclick='statusBattery(this)'><i id='exStatus' class=''></i>".($row->status == "0" ? "Active" : "InActive" )."</button>
                            </div>";
				return $action;
			})
			->addIndexColumn()
			->make(TRUE);
		return $batteries;
	}
}
