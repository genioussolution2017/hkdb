<?php

namespace App\Http\Controllers\Admin;

use Alert;
use App\Models\DataMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
	public function index()
	{
		return view('admin.login');
	}

	public function login(Request $request)
	{
		// Validate form input data
		$request->validate(
			[
				'email_address' => 'required|email',
				'password'      => 'required',
			]
		);

		// Get admin user details from database
		$admin_data = DataMaster::where(['DataEmail' => $request->input('email_address')])->first();
		if ( $admin_data && Hash::check($request->input('password'), $admin_data->DataLoginPwd) ) {
			session(['current_admin' => $admin_data]);
			return redirect('admin/dashboard');
		}
		else {
			Alert::error('Something went wrong, Please try again.')->persistent('Close');
			return back();
		}
	}

	public function logout()
	{
		session()->forget('current_admin');
		return redirect('/admin/login');
	}
}
