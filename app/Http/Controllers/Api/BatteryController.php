<?php

namespace App\Http\Controllers\Api;

use App\Models\Battery;
use App\Models\BatteryRentHistory;
use App\Models\UserWallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BatteryController extends Controller
{
	public function index(Request $request)
	{
		$batteries = Battery::select('*');
		if ( ! empty($request->station_id) ) {
			$batteries->where('station_id', $request->station_id);
		}
		if ( ! empty($request->unique_id) ) {
			$batteries->where('unique_id', $request->unique_id);
		}
		$batteries = $batteries->get();

		if ( ! empty($stations) && count($batteries->toArray()) > 0 ) {
			return response()->json(['status' => TRUE, 'batteries' => $batteries]);
		}
		else {
			return response()->json(['status' => FALSE, 'batteries' => []]);
		}
	}

	public function rentBattery(Request $request)
	{
		$request->validate([
			'battery_id' => 'required',
			'station_id' => 'required',
			'unique_id'  => 'required'
		]);

		$rent_battery = new BatteryRentHistory();
		$rent_battery->station_id = $request->input('station_id');
		$rent_battery->battery_id = $request->input('battery_id');
		$rent_battery->user_id = auth()->user()->id;
		$rent_battery->start_time = date('Y-m-d H:i:s');
		$save = $rent_battery->save();

		if ( $save ) {
			return response()->json(['status' => TRUE, 'message' => 'Battery rent details saved successfully.!']);
		}
		else {
			return response()->json(['status' => FALSE, 'message' => 'Something went wrong, Please try again.!']);
		}
	}

	public function returnBattery(Request $request)
	{
		$request->validate([
			'history_id' => 'required'
		]);

		$response = ['status' => FALSE, 'message' => 'Something went wrong, Please try again.!'];
		$history = BatteryRentHistory::find($request->input('history_id'));

		if ( ! empty($history) && isset($history->id) > 0 ) {
			$history->end_time = date('Y-m-d H:i:s');

			$used_hours = round((strtotime($history->end_time) - strtotime($history->start_time)) / 3600, 1);
			$history->total_amount = $used_hours * 10;
			$save = $history->save();
			if ( $save ) {
				$remain_amount = 0;
				$user_wallet = UserWallet::where('user_id', auth()->user()->id)->first();
				if ( ! empty($user_wallet) && isset($user_wallet->wallet_id) ) {
					$user_wallet->total_balance = $user_wallet->total_balance - $history->total_amount;
					if ( $user_wallet->total_balance < 0 ) {
						$remain_amount = abs($user_wallet->total_balance);
						$user_wallet->total_balance = 0;
					}
					$user_wallet->save();
				}
				$response = ['status' => TRUE, 'message' => 'Battery return status updated successfully.', 'remain_amount' => $remain_amount];
			}
		}
		return response()->json($response);
	}

	public function rentHistory()
	{
		$history = BatteryRentHistory::where(['user_id'=>auth()->user()->id])->get();
		if ( !empty($history) && count($history->toArray()) > 0) {
			return response()->json(['status' => TRUE, 'history' => $history]);
		}
		else {
			return response()->json(['status' => FALSE, 'message' => 'Something went wrong, Please try again.!']);
		}
	}
}
