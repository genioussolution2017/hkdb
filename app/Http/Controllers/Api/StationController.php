<?php

namespace App\Http\Controllers\Api;

use App\Models\Station;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StationTransction;

class StationController extends Controller
{
	public function index(Request $request)
	{
		$stations = Station::select('*');
		if(!empty($request->station_name))
		{
			$stations->where('station_name', $request->station_name);
		}
		if(!empty($request->latitude))
		{
			$stations->where('latitude', $request->latitude);
		}
		if(!empty($request->longitude))
		{
			$stations->where('latitude', $request->longitude);
		}
		$stations = $stations->get();
		if(!empty($stations) && count($stations->toArray()) > 0)
		{
			return response()->json(['status'=>TRUE, 'stations'=>$stations]);
		}
		else
		{
			return response()->json(['status'=>FALSE, 'stations'=>[]]);
		}
    }

    public function addStationTransction(Reques $request)
    {
    	$request->validate(
			[
				'station_id' => 'required',
				'amount' => 'required'
			]
		);

		$station_transaction = new StationTransction();
		$station_transaction->station_id = $request->input('station_id');
		$station_transaction->amount = $request->input('amount');
		$save = $station_transaction->save();

		if ( $save) {
			return response()->json(['status' => TRUE, 'message' => 'Transaction added successfully.']);
		}
		else {
			return response()->json(['status' => FALSE, 'message' => 'Data not found.']);
		}
    }
}
