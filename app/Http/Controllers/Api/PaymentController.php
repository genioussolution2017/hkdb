<?php

namespace App\Http\Controllers\Api;

use App\Models\Transaction;
use App\Models\UserWallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
	public function index()
	{
		$user_id = auth()->user()->id;
		$transactions = Transaction::where('user_id', $user_id)->get();
		if ( ! empty($transactions) && count($transactions->toArray()) > 0) {
			return response()->json(['status' => TRUE, 'transactions' => $transactions]);
		}
		else {
			return response()->json(['status' => FALSE, 'message' => 'Data not found.']);
		}
	}

	public function addTransactionDetails(Request $request)
	{
		$request->validate(
			[
				'transaction_id' => 'required',
				'amount' => 'required'
			]
		);

		$transaction = new Transaction();
		$transaction->user_id = auth()->user()->id;
		$transaction->transaction_id = $request->input('transaction_id');
		$transaction->amount = $request->input('amount');
		$transaction->payment_type = 'Paypal';
		$save = $transaction->save();

		if ( $save) {
			$user_wallet = UserWallet::where('user_id', auth()->user()->id)->first();
			if(empty($user_wallet))
			{
				$user_wallet = new UserWallet();
				$user_wallet->user_id = auth()->user()->id;
				$user_wallet->total_balance = $user_wallet->total_balance + $request->input('amount');
				$user_wallet->save();
			}
			else {
				UserWallet::where('user_id', auth()->user()->id)->update(['total_balance'=>$user_wallet->total_balance + $request->input('amount')]);
			}
			return response()->json(['status' => TRUE, 'message' => 'Transaction details added successfully.']);
		}
		else {
			return response()->json(['status' => FALSE, 'message' => 'Data not found.']);
		}
	}
}
