<?php

namespace App\Http\Controllers\Api;

use App\Models\VerifyOtp;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Twilio\Rest\Client;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;

class AuthController extends Controller
{
	/**
	 * User registration
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function register(Request $request)
	{
		$request->validate([
			'name'      => 'required|string',
			'email'     => 'required|string|email|unique:users',
			'mobile_no' => 'required|unique:users',
            'password'  => 'required'
		]);

		$user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->mobile_no = $request->input('mobile_no');
        $user->password = Hash::make($request->input('password'));
        $save = $user->save();

        if($save) {
            $code = str_random(6);
            $otp = new VerifyOtp();
            $otp->user_id = $user->id;
            $otp->code = $code;
            $otp->save();

            $client = new Client(env('TWILIO_ACCOUNT_SID'), env('TWILIO_AUTH_TOKEN'));

            // Use the client to do fun stuff like send text messages!
            $res = $client->messages->create(
                $user->mobile_no,
                array(
                    // 'from' => '+12029198588',
                    'from' => '+15005550006',
                    'body' => 'Your changed-up account authorization code is ' . $code . '.'
                )
            );
	
	        $tokenResult = $user->createToken('Personal Access Token');
	        $token = $tokenResult->token;
	        $token->save();

            return response()->json([
                'status' => true,
                'token' => $tokenResult->accessToken,
                'message' => 'Register Successfully.!!'
            ], 201);
        }
        return response()->json([
            'status' => false,
            'message' => 'Someting went wrong, Please try again.'
        ], 200);
	}

	/**
	 * Login user
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function login(Request $request)
	{
		$request->validate([
			'mobile_no' => 'required|string',
            'password'  => 'required'
		]);

		$user = User::where(['mobile_no'=>$request->input('mobile_no')])->first();
		if ( ! empty($user) && isset($user->id) ) {
		    if(Hash::check($request->input('password'), $user->password))
            {
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                $token->save();

                return response()->json([
                    'status' => true,
                    'access_token' => $tokenResult->accessToken,
                    'token_type'   => 'Bearer'
                ]);
            }

            return response()->json([
                'status' => false,
                'message' => 'Invalid username or password'
            ]);
		}
		else {
			return response()->json([
				'message' => 'Unauthorized'
			], 401);
		}
	}

	public function verifyOtp(Request $request)
	{
        $user = User::where('mobile_no', $request->input('mobile_no'))->first();
        if(empty($user) || !isset($user->id))
        {
            return response()->json([
                'status' => false,
                'message' => 'Invalid details'
            ]);
        }

		$otp_info = VerifyOtp::where('user_id', $user->id)->first();
		if(!empty($otp_info) && isset($otp_info->otp_id))
		{
			if($otp_info->code == $request->input('code'))
			{
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                $token->save();

                return response()->json([
                    'status' => true,
                    'access_token' => $tokenResult->accessToken,
                    'token_type'   => 'Bearer'
                ]);
			}
			else {
				return response()->json([
					'status' => FALSE,
					'message' => 'Invalid otp code'
				]);
			}
		}
		else {
			return response()->json([
				'status' => FALSE,
				'message' => 'Something went wrong, Please try again.!'
			]);
		}
	}

	/**
	 * Logout user (Revoke the token)
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function logout(Request $request)
	{
		$request->user()->token()->revoke();

		return response()->json([
			'message' => 'Successfully logged out'
		]);
	}

	/**
	 * Get the authenticated User
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function user(Request $request)
	{
		return response()->json($request->user());
	}

	public function forgotPassword(Request $request)
	{
		$request->validate([
			'type'     => 'required',
			'value' => 'required'
		]);
		$type = $request->input("type");
		$value = $request->input("value");

		$user_info = User::where($type, $value)->first();
		if(!empty($user_info) && isset($user_info->id)) {
				
				$code = str_random(6);
	            $otp = new VerifyOtp();
	            $otp->user_id = $user_info->id;
	            $otp->code = $code;
	            $otp->save();
			if($type == "mobile_no") {

	            $client = new Client(env('TWILIO_ACCOUNT_SID'), env('TWILIO_AUTH_TOKEN'));

	            // Use the client to do fun stuff like send text messages!
	            $res = $client->messages->create(
	                $user_info->mobile_no,
	                array(
	                    'from' => '+15005550006',
	                    'body' => 'Your changed-up account reset password code is ' . $code . '.'
	                )
	            );				
			}
			else {
				$user_info->notify(
                	new PasswordResetRequest($code)
            	);
			}
			return response()->json([
				'status' => true
			]);
			
		}
		else {
			return response()->json([
				'status' => FALSE,
				'message' => 'Something went wrong, Please try again.!'
			]);
		}
	}

	public function OtpVerify(Request $request)
	{
        $user = User::where('mobile_no', $request->input('value'))
        			->orWhere('email', $request->input('value'))
        			->first();
        if(empty($user) || !isset($user->id))
        {
            return response()->json([
                'status' => false,
                'message' => 'Invalid details'
            ]);
        }

		$otp_info = VerifyOtp::where('user_id', $user->id)
							->where('code', $request->input("otp"))
							->first();
		if(!empty($otp_info) && isset($otp_info->otp_id))
		{
			if($otp_info->code == $request->input('otp'))
			{
				VerifyOtp::where('otp_id', $otp_info->otp_id)->delete(); // done
                return response()->json([
                    'status' => true,
                    'user_id' => $user->id
                ]);
			}
			else {
				return response()->json([
					'status' => FALSE,
					'message' => 'Invalid otp code'
				]);
			}
		}
		else {
			return response()->json([
				'status' => FALSE,
				'message' => 'Something went wrong, Please try again.!'
			]);
		}
	}

	public function resetPassword(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'password' => 'required|string|confirmed'
        ]);
        $user = User::where('id', $request->input("user_id"))->first();
        if (!$user)
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.'
            ], 404);
        $user->password = bcrypt($request->password);
        $user->save();
        $user->notify(new PasswordResetSuccess($user));
        return response()->json($user);
    }
}
