<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
	return view('admin/login');
});

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
	Route::get('login', 'Admin\AuthController@index');
	Route::post('login', 'Admin\AuthController@login');
	Route::get('logout', 'Admin\AuthController@logout');

	Route::group(['middleware' => 'admin'], function () {
	    Route::get('/', function () {
	        return redirect('admin/login');
        });
		Route::get('dashboard', 'Admin\DashboardController@index');
		Route::get('users', 'Admin\DataMstController@index');
		Route::post('users', 'Admin\DataMstController@store');
		Route::match(['get', 'post'],'users/get', 'Admin\DataMstController@getUsers');
		Route::match(['get', 'post'],'users/create', 'Admin\DataMstController@store');
		Route::match(['get', 'post'],'users/update', 'Admin\DataMstController@update');
		/*Route::get('users', 'Admin\UsersController@index');
        Route::post('users/create', 'Admin\UsersController@create');
		Route::post('users/update', 'Admin\UsersController@update');
		Route::post('users/delete', 'Admin\UsersController@delete');

		Route::get('stations', 'Admin\StationsController@index');
		Route::post('stations/get', 'Admin\StationsController@getStation');
		Route::post('stations/create', 'Admin\StationsController@create');
		Route::post('stations/update', 'Admin\StationsController@update');
		Route::post('stations/delete', 'Admin\StationsController@delete');

		Route::get('batteries', 'Admin\BatteriesController@index');
		Route::post('batteries/get', 'Admin\BatteriesController@getBatteries');
		Route::post('batteries/create', 'Admin\BatteriesController@create');
		Route::post('batteries/update', 'Admin\BatteriesController@update');
		Route::post('batteries/delete', 'Admin\BatteriesController@delete');

		Route::get('promotions', 'Admin\PromotionsController@index');
		Route::post('promotions/get', 'Admin\PromotionsController@getPromotions');
		Route::post('promotions/create', 'Admin\PromotionsController@create');
		Route::post('promotions/update', 'Admin\PromotionsController@update');
		Route::post('promotions/delete', 'Admin\PromotionsController@delete');

		Route::get('rent-history', 'Admin\BatteryRentController@index');
		Route::post('rent-history/get', 'Admin\BatteryRentController@getRentbattery');

		Route::get('payment-transactions', 'Admin\UserPaymentController@index');
		Route::post('payment-transactions/get', 'Admin\UserPaymentController@getTransactions');*/

		Route::get('expenses', 'Admin\ExpenseController@index');
		Route::post('expenses', 'Admin\ExpenseController@store');
		Route::match(['get', 'post'],'expenses/get', 'Admin\ExpenseController@getExpenses');
		Route::match(['get', 'post'],'expenses/create', 'Admin\ExpenseController@store');
		Route::match(['get', 'post'],'expenses/update', 'Admin\ExpenseController@update');
		Route::post('expenses/delete', 'Admin\ExpenseController@destroy');

	});
});

Route::group(['prefix'=>'system'], function () {
    Route::get('logs', '\Melihovv\LaravelLogViewer\LaravelLogViewerController@index');
});
