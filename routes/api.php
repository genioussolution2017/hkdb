<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['prefix' => 'auth'], function () {
	Route::post('login', 'Api\AuthController@login');
	Route::post('register', 'Api\AuthController@register');
	Route::post('forgot-password', 'Api\AuthController@forgotPassword'); // pela lakhelu e baraber che
	Route::post('verify-password', 'Api\AuthController@OtpVerify'); // pela lakhelu e baraber che
	Route::post('reset-password', 'Api\AuthController@resetPassword'); // pela lakhelu e baraber che

	Route::group(['middleware' => 'auth:api'], function () {
		Route::get('logout', 'Api\AuthController@logout');
		Route::get('user', 'Api\AuthController@user');
		Route::post('verify-otp', 'Api\AuthController@verifyOtp');
	});
});

Route::group(['middleware' => 'auth:api'], function () {
	Route::post('stations', 'Api\StationController@index');
	Route::post('batteries', 'Api\BatteryController@index');

	Route::post('rent-battery', 'Api\BatteryController@rentBattery');
	Route::post('return-battery', 'Api\BatteryController@returnBattery');
	Route::get('rent-history', 'Api\BatteryController@rentHistory');

	Route::get('user/transactions', 'Api\PaymentController@index');
	Route::post('user/transactions/add', 'Api\PaymentController@addTransactionDetails');

	Route::post('station-transaction', 'Api\StationController@addStationTransction');
});
