@extends('layouts.admin')

@section('content')
	<div class="container-fluid px-xl-5">
		<section class="py-5">
			<div class="row">
	            <div class="col-lg-12 mb-5">
	                <div class="card">
	                    <div class="card-header">
	                    	<div class="row">
		                    	<div class="col-lg-6">
		                        	<h3 class="h6 text-uppercase mb-0">Battery on rent</h3>
		                    	</div>
		                    </div>
	                    </div>
	                    <div class="card-body" id="showPromotion">
	                        <table class="table table-striped table-sm card-text table table-bordered table-hover table-striped table-td-valign-middle" id="batteryonrent_table">
							    <thead>
							        <tr>
							            <th>User Info</th>
							            <th>Station Name</th>
							            <th>Battery Id</th>
							            <th>Power Info</th>
							            <th>Start Time</th>
							            <th>End Time</th>
							            <th>Rent Status</th>
							        </tr>
							    </thead>
							    <tbody>
							    </tbody>
							</table>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	var oTable;
    $(document).ready(function () {
        oTable = $('#batteryonrent_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: BASE_URL + 'rent-history/get',
				type: 'POST',
			},					
        	columns: [
                {
                	render: function(data, meta, row) {
            			return '<a href="mailto:'+row.email+'">'+row.name+'</a><br>'+row.mobile_no;
                	}
                },
                {data: 'station_name', name: 'station_name', searchable: true},
                {data: 'unique_id', name: 'unique_id', searchable: true},
                {data: 'power_info', name: 'power_info'},
                {data: 'start_time', name: 'start_time'},
                {data: 'end_time', name: 'end_time'},
                {data: 'rstatus', name: 'rstatus', orderable: false, searchable: false}
            ],
            deferRender: true,
            lengthMenu: [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
            pageLength: 10                   
        });
    });
</script>
@endsection