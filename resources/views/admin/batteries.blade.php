@extends('layouts.admin')

@section('content')
	<div class="container-fluid px-xl-5">
		<section class="py-5">
			<div class="row">
	            <div class="col-lg-12 mb-5">
	                <div class="card">
	                    <div class="card-header">
	                    	<div class="row">
		                    	<div class="col-lg-6">
	                        		<h3 class="h6 text-uppercase mb-0">Batteries</h3>
		                    	</div>
		                    	<div class="col-lg-6" >
		                    		<button style="float: right;" type="button" onclick="$('#AddUpdateBattery').show();$('#showBattery').hide();resetForm('batteryForm');$('#batteryForm').attr('action',BASE_URL+'batteries/create');" class="btn btn-primary btn-sm">Add</button>
		                    	</div>
		                    </div>
	                    </div>
	                    <div class="card-body" id="showBattery" >
	                        <table class="table table-striped table-sm card-text table table-bordered table-hover table-striped table-td-valign-middle" id="battery_table">
							    <thead>
							        <tr>
							            <th>Battery Name</th>
							            <th>Station Name</th>
							            <th>Battery Info</th>
							            <th>Description</th>
							            <th class="status">Status</th>
							            <th>Action</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							        </tr>
							    </tbody>
							</table>
	                    </div>
	                    <div class="card-body" id="AddUpdateBattery" style="display: none;">
	                        <form class="form-horizontal" method="post" action="{{url('admin/batteries/create')}}" id="batteryForm" data-parsley-validate="">
	                        	@csrf
	                        	<input hidden name="battery_id" id="battery_id">
	                        	<div class="form-group row">
	                                <label class="col-md-3 form-control-label">Select Station</label>
	                                <div class="col-md-6 select mb-3">
	                                    <select class="form-control" name="available_at_station" id="available_at_station" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please select station">
	                                    	<option value="" >Select Station</option>
	                                    	@if (!empty($stations))
	                                    		@foreach($stations as $station)
	                                        		<option value="{{$station->station_id}}" >{{$station->station_name}}</option>
	                                        	@endforeach
	                                        @endif
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Battery Name</label>
	                                <div class="col-md-6">
	                                    <input type="text" class="form-control" placeholder="Power Infor" name="battery_name" id="battery_name" data-parsley-trigger="change keyup keydown" parsley-type="text" required="" data-parsley-required-message="Please enter battery name">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Power Infor</label>
	                                <div class="col-md-6">
	                                    <input type="text" class="form-control" placeholder="Power Infor" name="power_info" id="power_info" data-parsley-trigger="change keyup keydown" parsley-type="text" required="" data-parsley-required-message="Please enter power Info">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Battery Description</label>
	                                <div class="col-md-6">
	                                	<textarea class="form-control" rows="3" cols="3" placeholder="Battery Description" name="battery_desc" id="battery_desc" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please enter battery description"></textarea>
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <div class="col-md-9 ml-auto">
	                                    <button type="button" onclick="$('#AddUpdateBattery').hide();$('#showBattery').show();" class="btn btn-secondary">Cancel</button>
	                                    <button type="submit" class="btn btn-primary">Save changes</button>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		var batteryTable;
        $(document).ready(function () {
            batteryTable = $('#battery_table').DataTable({
                processing: true,
                serverSide: true,
	            order: [],
                ajax: {
                    url: BASE_URL + 'batteries/get',
					type: 'POST',
				},					
            	columns: [
                    {data: 'battery_name', name: 'battery_name', searchable: true},
                    {data: 'station_name', name: 'station_name', searchable: true},
                    {data: 'power_info', name: 'power_info', searchable: true},
                    {data: 'battery_desc', name: 'battery_desc', searchable: true},
                    {data: 'status', name: 'status', searchable: true},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                "columnDefs": [{
		            "render": function (data, type, row, meta) {
		                if (type === 'display') {
		                    return (row.status == "1" ? "ACTIVE" : "INACTIVE");
		                }
		                else return "";
		            },
		            "targets": "status"
		        }],
                deferRender: true,
                lengthMenu: [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
                pageLength: 10                   
            });
        });

        function editBattery(t){
        	var data = batteryTable.row($(t).parents('tr')).data();
        	$("#battery_id").val(data.battery_id);
        	$("#available_at_station").val(data.available_at_station).change();
        	$("#battery_name").val(data.battery_name);
        	$("#power_info").val(data.power_info);
        	$("#battery_desc").val(data.battery_desc);
        	$("#batteryForm").attr('action',BASE_URL+'batteries/update');
        	$("#AddUpdateBattery").show();
        	$('#showBattery').hide();
        }

        function deleteBattery(t){
        	var battery_id = batteryTable.row($(t).parents('tr')).data().battery_id;
			swal({
				title: "Confirmation",
				text: "Are you sure you want to delete this battery details?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((isConfirm) => {
				if (isConfirm) {
		            $.ajax({
	                	url: BASE_URL+'batteries/delete',
	                	type: "POST",
	                	data: {
	                		battery_id: battery_id
	                	},
	                	success: function (response) {
	                		if(response.status == true){
	                    		swal("Done!",response.message,"success");
	                		}else{
	                			swal("Cancelled",response.message,"error");
	                		}
	                    	batteryTable.ajax.reload();
	                	}
		            });
	          	}
	          	else{
	                swal("Cancelled", "", "error");
	          	}
			});
		}

		function statusBattery(t){
        	var battery_id = batteryTable.row($(t).parents('tr')).data().battery_id;
        	var status = batteryTable.row($(t).parents('tr')).data().status;
        	var status1 = "";
        	if(status == "0") {
        		status = 1;
        		status1="Active";
        	}else{
        		status = 0;
        		status1="In-active";
        	}
			swal({
				title: "Confirmation",
				text: "Are you sure you want to change battery status to "+status1+"?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((isConfirm) => {
				if (isConfirm) {
		            $.ajax({
	                	url: BASE_URL+'batteries/update',
	                	type: "POST",
	                	data: {
	                		battery_id: battery_id,
	                		status : status
	                	},
	                	success: function () {
	                    	swal("Done!",status1+" succesfully!","success");
	                    	//console.log(batteryTable);
	                    	batteryTable.ajax.reload();
	                	}
		            });
	          	}
	          	else{
	                swal("Cancelled", "", "error");
	          	}
			});
	    }

	    
	</script>
@endsection
<!-- validation easily set thay e mukjo -->