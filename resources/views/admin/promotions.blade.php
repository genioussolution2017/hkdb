@extends('layouts.admin')

@section('content')
	<div class="container-fluid px-xl-5">
		<section class="py-5">
			<div class="row">
	            <div class="col-lg-12 mb-5">
	                <div class="card">
	                    <div class="card-header">
	                    	<div class="row">
		                    	<div class="col-lg-6">
		                        	<h3 class="h6 text-uppercase mb-0">Promotions</h3>
		                    	</div>
		                    	<div class="col-lg-6" >
		                    		<button style="float: right;" type="button" onclick="$('#AddUpdatePromotion').show();$('#showPromotion').hide();resetForm('stationForm');$('#stationForm').attr('action',BASE_URL+'promotions/create');" class="btn btn-primary btn-sm">Add</button>
		                    	</div>
		                    </div>
	                    </div>
	                    <div class="card-body" id="showPromotion">
	                        <table class="table table-striped table-sm card-text table table-bordered table-hover table-striped table-td-valign-middle" id="promotion_table">
							    <thead>
							        <tr>
							            <th>Promocode</th>
							            <th>Discount</th>
							            <th class="start_date">Start Date</th>
							            <th class="end_date">End Date</th>
							            <th class="status">Status</th>
							            <th>Action</th>
							        </tr>
							    </thead>
							    <tbody>
							    </tbody>
							</table>
	                    </div>
	                    <div class="card-body" id="AddUpdatePromotion" style="display: none;">
	                        <form class="form-horizontal" method="post" action="{{url('admin/promotions/create')}}" id="stationForm" data-parsley-validate="">
	                        	@csrf
	                        	<input hidden name="promotion_id" id="promotion_id">
	                        	<div class="row">
		                        	<div class="col-lg-6">
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Promocode</label>
			                                <div class="col-md-9">
			                                    <input type="text" class="form-control" placeholder="Promocode" name="promocode" id="promocode" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please enter promocode"  >
			                                </div>
			                            </div>
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Discount</label>
			                                <div class="col-md-9">
			                                	<input type="text" class="form-control" placeholder="Discount" name="discount" id="discount" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please enter discount" data-parsley-type="number">
			                                </div>
			                            </div>
		                        	</div>
		                        	<div class="col-lg-6">
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Start Date</label>
			                                <div class="col-md-9">
			                                	<input type="text" class="datetime form-control" placeholder="start_date" name="start_date" id="start_date" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please select date" readonly="readonly">
			                                </div>
			                            </div>
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">End Date</label>
			                                <div class="col-md-9">
			                                    <input type="text" class="datetime form-control" placeholder="end_date" name="end_date" id="end_date" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please select date" readonly="readonly">
			                                </div>
			                            </div>
		                        	</div>		                        	
		                        </div>
	                            <div class="form-group row">
	                                <div class="col-md-9 ml-auto">
	                                    <button type="button" onclick="$('#AddUpdatePromotion').hide();$('#showPromotion').show();" class="btn btn-secondary">Cancel</button>
	                                    <button type="submit" class="btn btn-primary">Save changes</button>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	var oTable;
    $(document).ready(function () {
        oTable = $('#promotion_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: BASE_URL + 'promotions/get',
				type: 'POST',
			},					
        	columns: [
                {data: 'promocode', name: 'promocode'},
                {data: 'discount', name: 'discount', searchable: true},
                {data: 'start_date', name: 'start_date', searchable: true},
                {data: 'end_date', name: 'end_date', searchable: true},
                {data: 'status', name: 'status', orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "columnDefs": [{
	            "render": function (data, type, row, meta) {
	                if (type === 'display') {
	                    return (row.status == "1" ? "ACTIVE" : "INACTIVE");
	                }
	                else return "";
	            },
	            "targets": "status"
	        }],
            deferRender: true,
            lengthMenu: [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
            pageLength: 10                   
        });
    });
    function editStation(t){
    	var data = oTable.row($(t).parents('tr')).data();
    	$("#promotion_id").val(data.promotion_id);

    	$("#promocode").val(data.promocode);
    	$("#discount").val(data.discount);
    	$("#start_date").val(data.start_date);
    	$("#end_date").val(data.end_date);
    	$("#stationForm").attr('action',BASE_URL+'promotions/update');
    	$("#AddUpdatePromotion").show();
    	$('#showPromotion').hide();
    }

    function deleteStation(t){
    	var promotion_id = oTable.row($(t).parents('tr')).data().promotion_id;
		swal({
			title: "Confirmation",
			text: "Are you sure you want to delete this promocode details?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((isConfirm) => {
			if (isConfirm) {
	            $.ajax({
                	url: BASE_URL+'promotions/delete',
                	type: "POST",
                	data: {
                		promotion_id: promotion_id
                	},
                	success: function (response) {
                		if(response.status == true){
                    		swal("Done!",response.message,"success");
                		}else{
                			swal("Cancelled",response.message,"error");
                		}
                    	//console.log(oTable);
                    	oTable.ajax.reload();
                	}
	            });
          	}
          	else{
                swal("Cancelled", "", "error");
          	}
		});
    }

    function statusStation(t){
    	var promotion_id = oTable.row($(t).parents('tr')).data().promotion_id;
    	var status = oTable.row($(t).parents('tr')).data().status;
    	var status1 = "";
    	if(status == "0") {
    		status = 1;
    		status1="Active";
    	}else{
    		status = 0;
    		status1="In-active";
    	}
		swal({
			title: "Confirmation",
			text: "Are you sure you want to change promocode status to "+status1+"?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((isConfirm) => {
			if (isConfirm) {
	            $.ajax({
                	url: BASE_URL+'promotions/update',
                	type: "POST",
                	data: {
                		promotion_id: promotion_id,
                		status : status
                	},
                	success: function () {
                    	swal("Done!",status1+" succesfully!","success");
                    	oTable.ajax.reload();
                	}
	            });
          	}
          	else{
                swal("Cancelled", "", "error");
          	}
		});
    }
</script>
@endsection