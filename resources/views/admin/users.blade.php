@extends('layouts.admin')

@section('content')

	<div class="container-fluid px-xl-5">
		<section class="py-5">
			<div class="row">
	            <div class="col-lg-12 mb-5">
	                <div class="card">
	                    <div class="card-header">
	                    	<div class="row">
		                    	<div class="col-lg-6">
	                        		<h3 class="h6 text-uppercase mb-0" id="titleUser">Users List</h3>
		                    	</div>
		                    	<div class="col-lg-6" >
		                    		<button style="float: right;" type="button" onclick="$('#AddUpdateUser').show();$('#showUser').hide();resetForm('userForm');$('#userForm').attr('action',BASE_URL+'users/create');$('#titleUser').html('Add User');" class="btn btn-primary btn-sm">Add</button>
		                    	</div>
		                    </div>
	                    </div>
	                    <div class="card-body" id="showUser">
	                        <table class="table table-striped table-sm card-text table table-bordered table-hover table-striped table-td-valign-middle" id="user_table">
							    <thead>
							        <tr>
							            <th>User Name</th>
							            <th>Mobile No</th>
							            <th>Email</th>
							            <th>Balance</th>
							            <th>Action</th>
							        </tr>
							    </thead>
							    <tbody>
							       
							    </tbody>
							</table>
	                    </div>
	                    <div class="card-body"  id="AddUpdateUser" style="display: none;">
	                        <form class="form-horizontal" method="post" action="{{url('admin/users/create')}}" id="userForm" data-parsley-validate="">
	                        	@csrf
	                        	<input hidden name="id" id="id">
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Full Name</label>
	                                <div class="col-md-6">
	                                    <input type="text" class="form-control" placeholder="Full Name" name="name" id="name" data-parsley-trigger="change keyup keydown" required="" data-required="true" data-parsley-required-message="Please enter user name">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Mobile No</label>
	                                <div class="col-md-6">
	                                    <input type="text" class="form-control" placeholder="Mobile No" name="mobile_no" id="mobile_no" data-parsley-trigger="change keyup keydown" required="" data-required="true" data-parsley-type="digits" data-parsley-required-message="Please mobile no">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Email</label>
	                                <div class="col-md-6">
	                                    <input type="text" class="form-control" placeholder="Email address" name="email" id="email" data-parsley-trigger="change keyup keydown" required="" data-required="true" data-parsley-type="email" data-parsley-required-message="Please enter your email">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Password</label>
	                                <div class="col-md-6">
	                                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" data-parsley-trigger="change keyup keydown" data-equalto="#eqalToModel" required="" data-required="true"  data-parsley-type="password" data-parsley-required-message="Please enter your password">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <div class="col-md-9 ml-auto">
	                                    <button type="button" onclick="$('#AddUpdateUser').hide();$('#showUser').show();$('#titleUser').html('User List');" class="btn btn-secondary">Cancel</button>
	                                    <button type="submit" class="btn btn-primary">Save changes</button>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>

@endsection
@section('script')
	<script type="text/javascript">
		var oTable;
        $(document).ready(function () {
            oTable = $('#user_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: BASE_URL + 'users/get',
					type: 'POST',
				},					
            	columns: [
                    {data: 'name', name: 'name', searchable: true},
                    {data: 'email', name: 'email', searchable: true},
                    {data: 'mobile_no', name: 'mobile_no', searchable: true},
                    {data: 'total_balance', name: 'total_balance', searchable: true},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                "columnDefs": [{
		            "render": function (data, type, row, meta) {
		                if (type === 'display') {
		                    return (row.status == "1" ? "ACTIVE" : "INACTIVE");
		                }
		                else return "";
		            },
		            "targets": "status"
		        }],
                deferRender: true,
                lengthMenu: [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
                pageLength: 10                   
            });
        });

        function editUser(t){
	    	var data = oTable.row($(t).parents('tr')).data();
	    	$("#id").val(data.id);

	    	$("#name").val(data.name);
	    	$("#mobile_no").val(data.mobile_no);
	    	$("#email").val(data.email);
	    	$("#userForm").attr('action',BASE_URL+'users/update');
	    	$("#AddUpdateUser").show();
	    	$('#showUser').hide();
	    	$("#titleUser").html("Edit User");
	    }
	    
		function statusUser(t){
        	var id = oTable.row($(t).parents('tr')).data().id;
        	var status = oTable.row($(t).parents('tr')).data().is_block;
        	var status1 = "";
        	if(status == "0") {
        		status = 1;
        		status1 = "Un-Block";
        	}else{
        		status = 0;
        		status1 = "Block";
        	}
			swal({
				title: "Confirmation",
				text: "Are you sure you want to "+status1+" user?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((isConfirm) => {
				if (isConfirm) {
		            $.ajax({
	                	url: BASE_URL+'users/update',
	                	type: "POST",
	                	data: {
	                		id: id,
	                		is_block : status
	                	},
	                	success: function () {
	                    	swal("Done!","User "+status1+" succesfully!","success");
	                    	oTable.ajax.reload();
	                	}
		            });
	          	}
	          	else{
	                swal("Cancelled", "", "error");
	          	}
			});
	    }	    
	</script>
@endsection