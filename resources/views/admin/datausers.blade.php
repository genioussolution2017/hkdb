@extends('layouts.admin')

@section('content')
	
		<!-- begin page-header -->
		<!-- <h1 class="page-header">Users Data <small>header small text goes here...</small></h1> -->
		<!-- end page-header -->
		<!-- begin panel -->
		<div class="row">
			<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 ui-sortable">
				<div class="panel panel-inverse">
					<!-- begin panel-heading -->
					<div class="panel-heading">
						<h4 class="panel-title" id="titleUser">Users Data</h4>
						<div class="panel-heading-btn">
							<button type="button" onclick="$('#AddUpdateUser').show();$('#showUserList').hide();resetForm('data_users_addUpdate');$('#data_users_addUpdate').attr('action',BASE_URL+'users/create');$('#titleUser').html('Add User');" class="btn btn-sm btn-icon btn-circle btn-default"><i class="fa fa-plus"></i></button>&nbsp;&nbsp;
							<button type="button" onclick="$('#AddUpdateUser').hide();$('#showUserList').show();$('#titleUser').html('Users Data');" class="btn btn-sm btn-icon btn-circle btn-success"><i class="fa fa-redo"></i></button>
							<!-- <a href="javascript:;" onclick="$('#AddUpdateUser').show();$('#showUser').hide();resetForm('userForm');$('#userForm').attr('action',BASE_URL+'users/create');$('#titleUser').html('Add User');" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-plus"><i class="fa fa-plus"></i></a> -->
							<!-- <a href="{{ url('admin/get') }}" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a> -->
							<!-- <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> -->
						</div>
					</div>
					<!-- end panel-heading -->
					<!-- begin panel-body -->
					<div class="panel-body" id="showUserList" >
						<table id="data_users" class="table table-striped table-bordered table-td-valign-middle desktop tablet" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>Org Name</th>
						            <th>USer Name</th>
									<th>Email</th>
									<th>Contact No</th>
									<th>Login Type</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>							       
						    </tbody>
						</table>
					</div>
					<!-- <div class="panel panel-inverse" id="AddUpdateUser"  data-sortable-id="form-validation-1" style="display: none;"> -->
						<!-- begin panel-body -->
						<div class="panel-body" id="AddUpdateUser" style="display: none;">
							<form class="form-horizontal" id="data_users_addUpdate" enctype="multipart/form-data" data-parsley-validate="" method="post" action="" name="userfrom" novalidate="">
								@csrf
								<input hidden name="id" id="DataID">
								<div class="row">
									<div class="col-xl-6 col-md-6 col-sm-6 col-xs-12 ui-sortable">
										<!-- <button type="button" class="btn dropdown-toggle btn-white" data-toggle="dropdown" role="combobox" aria-owns="bs-select-2" aria-haspopup="listbox" aria-expanded="false" title="Select a Country">
											<div class="filter-option">
												<div class="filter-option-inner">
													<div class="filter-option-inner-inner">Select a Country
													</div>
												</div> 
											</div>
										</button> -->
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label">Select Type <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<select class="form-control" id="login_type" name="DataLoginTypeID" data-parsley-required="true">
													<option value="" >Select Type</option>
			                                    	@if (!empty($loginType))
			                                    		@foreach($loginType as $Ltype)
			                                        		<option value="{{$Ltype->MstID}}" >{{$Ltype->MstName}}</option>
			                                        	@endforeach
			                                        @endif
												</select>
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label">Select Title <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<select class="form-control" id="data_title" name="DataContactTitle" data-parsley-required="true">
													<option value="" >Select Title</option>
			                                    	@if (!empty($userDataTitle))
			                                    		@foreach($userDataTitle as $udTitle)
			                                        		<option value="{{$udTitle->MstID}}" >{{$udTitle->MstName}}</option>
			                                        	@endforeach
			                                        @endif
												</select>
											</div>
										</div>
										<div id="compDetails" >
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-sm-4 col-form-label" for="DataCd">Company code <span class="text-danger">*</span> :</label>
												<div class="col-md-8 col-sm-8">
													<input type="text" class="form-control" placeholder="Company code" name="DataCd" id="company_code" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter company code">
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-sm-4 col-form-label" for="DataName">Company Name <span class="text-danger">*</span> :</label>
												<div class="col-md-8 col-sm-8">
													<input type="text" class="form-control" placeholder="Company Name" name="DataName" id="company_name" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter user name">
												</div>
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataContactName1">Contact Name <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Full Name" name="DataContactName1" id="data_name" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter user name">
												<input type="hidden" name="DataContactName" id="data_name1">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataLoginContactNo">Contact <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Contact No" name="DataLoginContactNo" id="data_contact_no" data-parsley-trigger="change keyup keydown" data-parsley-type="digits" data-parsley-required-message="Please mobile no">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataEmail">Email <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Email" name="DataEmail" id="data_email" data-parsley-trigger="change keyup keydown" data-parsley-type="email" data-parsley-required-message="Please enter your email">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataAddr1">Address 1 <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Addess 1" name="DataAddr1" id="data_address1" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter addess1">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataAddr2">Address 2 <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Addess 2" name="DataAddr2" id="data_address2" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter addess2">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataAddrState">State <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="State" name="DataAddrState" id="data_state" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter state">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataAddrCity">City <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="City" name="DataAddrCity" id="data_city" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter city">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataAddrPinCode">Pincode <span class="text-danger">*</span> :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Pincode" name="DataAddrPinCode" id="data_pincode" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter address pincode">
											</div>
										</div>
									</div>
									<div class="col-xl-6 col-md-6 col-sm-6 col-xs-12 ui-sortable">
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataProfileImage">Profile Image :</label>
											<div class="col-md-8 col-sm-8">
												<input type="file" class="form-control" name="DataProfileImage" id="data_profile_image"  data-parsley-max-file-size="3072">
												<small class="text-danger">Please provide a file smaller than 3 MB:</small>
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataDesktopImage">Desktop Image :</label>
											<div class="col-md-8 col-sm-8">
												<input type="file" class="form-control" name="DataDesktopImage" id="data_desktop_image"  data-parsley-max-file-size="3072">
												<small class="text-danger">Please provide a file smaller than 3 MB:</small>
											</div>
										</div>
										<!-- DATA ALTER -->
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label">Select Alter Title :</label>
											<div class="col-md-8 col-sm-8">
												<select class="form-control" id="data_alt_title" name="DataAltContactTitle" data-parsley-required="false">
													<option value="" >Select Alter Title</option>
			                                    	@if (!empty($userDataTitle))
			                                    		@foreach($userDataTitle as $udTitle)
			                                        		<option value="{{$udTitle->MstID}}" >{{$udTitle->MstName}}</option>
			                                        	@endforeach
			                                        @endif
												</select>
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataAltContactName">Alter Name :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Full Alter Name" name="DataAltContactName" id="data_alt_name" data-parsley-trigger="change keyup keydown" data-required="false" data-parsley-required-message="Please enter user name">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataAltContactTitleNo">Alter Contact No :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Alter contact no" name="DataAltContactTitleNo" id="data_alt_contactno" data-parsley-trigger="change keyup keydown" data-required="false" data-parsley-type="digits" data-parsley-required-message="Please mobile no">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataAltContactEmail">Alter Email :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Email" name="DataAltContactEmail" id="data_alt_email" data-parsley-trigger="change keyup keydown" data-required="false" data-parsley-type="email" data-parsley-required-message="Please enter your email">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataTinNo">TinNo :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="TinNo" name="DataTinNo" id="data_tinno" data-parsley-trigger="change keyup keydown" data-required="false" data-parsley-required-message="Please enter Tinno">
											</div>
										</div>
										<div class="form-group row m-b-15">
											<label class="col-md-4 col-sm-4 col-form-label" for="DataRemarks">Remarks :</label>
											<div class="col-md-8 col-sm-8">
												<input type="text" class="form-control" placeholder="Remarks" name="DataRemarks" id="data_remarks" data-parsley-trigger="change keyup keydown" data-required="false" data-parsley-required-message="Please enter Remarks">
											</div>
										</div>
									</div>
								</div>
								<div class="row">									
									<div class="form-group row m-b-0">
										<label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
										<div class="col-md-8 col-sm-8">
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
						<!-- end panel-body -->
					</div>
				</div>
			</div>
		</div>
		<!-- end panel -->
		

		
	<!-- <div class="container-fluid px-xl-5">
		<section class="py-5">
			<div class="row">
	            <div class="col-lg-12 mb-5">
	                <div class="card">
	                    <div class="card-header">
	                    	<div class="row">
		                    	<div class="col-lg-6">
	                        		<h3 class="h6 text-uppercase mb-0" id="titleUser">Users List</h3>
		                    	</div>
		                    	<div class="col-lg-6" >
		                    		<button style="float: right;" type="button" onclick="$('#AddUpdateUser').show();$('#showUser').hide();resetForm('userForm');$('#userForm').attr('action',BASE_URL+'users/create');$('#titleUser').html('Add User');" class="btn btn-primary btn-sm">Add</button>
		                    	</div>
		                    </div>
	                    </div>
	                    
	                    <div class="card-body"  id="AddUpdateUser" style="display: none;">
	                        <form class="form-horizontal" method="post" action="{{url('admin/users/create')}}" id="userForm" data-parsley-validate="">
	                        	@csrf
	                        	<input hidden name="id" id="id">
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Full Name</label>
	                                <div class="col-md-6">
	                                    <input type="text" class="form-control" placeholder="Full Name" name="name" id="name" data-parsley-trigger="change keyup keydown" data-parsley-required-message="Please enter user name">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Mobile No</label>
	                                <div class="col-md-6">
	                                    <input type="text" class="form-control" placeholder="Mobile No" name="mobile_no" id="mobile_no" data-parsley-trigger="change keyup keydown" data-parsley-type="digits" data-parsley-required-message="Please mobile no">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Email</label>
	                                <div class="col-md-6">
	                                    <input type="text" class="form-control" placeholder="Email address" name="email" id="email" data-parsley-trigger="change keyup keydown" data-parsley-type="email" data-parsley-required-message="Please enter your email">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-3 form-control-label">Password</label>
	                                <div class="col-md-6">
	                                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" data-parsley-trigger="change keyup keydown" data-equalto="#eqalToModel"  data-parsley-type="password" data-parsley-required-message="Please enter your password">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <div class="col-md-9 ml-auto">
	                                    <button type="button" onclick="$('#AddUpdateUser').hide();$('#showUser').show();$('#titleUser').html('User List');" class="btn btn-secondary">Cancel</button>
	                                    <button type="submit" class="btn btn-primary">Save changes</button>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div> -->

@endsection
@section('script')
	<script type="text/javascript">
		var oTable;
        $(document).ready(function () {
        	// console.log("{{ url('users/get') }}");
            oTable = $('#data_users').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: BASE_URL+'users/get',
                    // url: "{{ url('users/get') }}",
					type: 'GET',
				},					
            	columns: [
            		{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'org_name', name: 'org_name', searchable: true},
                    {data: 'user_name', name: 'user_name', searchable: true},
                    {data: 'email', name: 'email', searchable: true},
                    {data: 'contact_no', name: 'contact_no', searchable: true},
                    {data: 'login_type', name: 'login_type', searchable: true},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                /*"columnDefs": [{
		            "render": function (data, type, row, meta) {
		                if (type === 'display') {
		                    return (row.valid_status == "Y" ? "ACTIVE" : "INACTIVE");
		                }
		                else return "";
		            },
		            "targets": "login_type"
		        }],*/
                deferRender: true,
                lengthMenu: [[10, 15, 25, 50, 100, 1000], [10, 15, 25, 50, 100, 1000]],
                pageLength: 15                   
            });
        });

        function editDataUser(t){
	    	var data = oTable.row($(t).parents('tr')).data();
	    	$("#DataID").val(data.data_id);

	    	$("#login_type").val(data.login_type_id);
	    	$("#data_title").val(data.data_title);
	    	$("#company_code").val(data.company_cd);
	    	$("#company_name").val(data.company_name);
	    	$("#data_name").val(data.user_name);
	    	$("#data_contact_no").val(data.contact_no);
	    	$("#data_email").val(data.email);
	    	$("#data_address1").val(data.DataAddr1);
	    	$("#data_address2").val(data.DataAddr2);
	    	$("#data_state").val(data.DataAddrState);
	    	$("#data_city").val(data.DataAddrCity);
	    	$("#data_pincode").val(data.DataAddrPinCode);	    	
	    	$("#data_alt_title").val(data.data_alt_title);
	    	$("#data_alt_email").val(data.data_alt_email);
	    	$("#data_alt_contactno").val(data.data_alt_contactno);
	    	$("#data_alt_name").val(data.data_alt_name);	    	
	    	$("#data_tinno").val(data.t_no);
	    	$("#data_remarks").val(data.data_remarks);
	    	$("#data_alt_title").val(data.data_alt_title);

	    	$("#data_users_addUpdate").attr('action',BASE_URL+'users/update');
	    	$("#AddUpdateUser").show();
	    	$('#showUserList').hide();
	    	$("#titleUser").html("Edit User");
	    }
	    $(document).on('change',"#company_name",function(e){
	        var loginID = $('#login_type').val();
	        if(loginID != "10" && loginID != "11" && loginID != "12"){
	        	$("#data_name").val($("#company_name").val());
	        	$("#data_name1").val($("#company_name").val());
	        	$("#data_name").prop("disabled",true);
	        }else{
	        	$("#data_name1").val('');
	        	$('#data_name').prop("disabled",false);
	        }
	    });
	    $(document).on('change',"#login_type",function(e){
	        var loginID = $('#login_type').val();
	        if(loginID != "10" && loginID != "11" && loginID != "12"){
	        	$("#data_name").val($("#company_name").val());
	        	$("#data_name1").val($("#company_name").val());
	        	$("#data_name").prop("disabled",true);
	        }else{
	        	$("#data_name1").val('');
	        	$('#data_name').prop("disabled",false);
	        }
	    });
	    
		function statusUser(t){
        	var id = oTable.row($(t).parents('tr')).data().id;
        	var status = oTable.row($(t).parents('tr')).data().is_block;
        	var status1 = "";
        	if(status == "0") {
        		status = 1;
        		status1 = "Un-Block";
        	}else{
        		status = 0;
        		status1 = "Block";
        	}
			swal({
				title: "Confirmation",
				text: "Are you sure you want to "+status1+" user?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((isConfirm) => {
				if (isConfirm) {
		            $.ajax({
	                	url: BASE_URL+'users/update',
	                	type: "POST",
	                	data: {
	                		id: id,
	                		is_block : status
	                	},
	                	success: function () {
	                    	swal("Done!","User "+status1+" succesfully!","success");
	                    	oTable.ajax.reload();
	                	}
		            });
	          	}
	          	else{
	                swal("Cancelled", "", "error");
	          	}
			});
	    }	    
	</script>
@endsection