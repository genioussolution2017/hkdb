@extends('layouts.admin')

@section('content')
	<div class="container-fluid px-xl-5">
		<section class="py-5">
			<div class="row">
	            <div class="col-lg-12 mb-5">
	                <div class="card">
	                    <div class="card-header">
	                    	<div class="row">
		                    	<div class="col-lg-6">
		                        	<h3 class="h6 text-uppercase mb-0">Stations</h3>
		                    	</div>
		                    	<div class="col-lg-6" >
		                    		<button style="float: right;" type="button" onclick="$('#AddUpdateStation').show();$('#showStation').hide();resetForm('stationForm');$('#stationForm').attr('action',BASE_URL+'stations/create');" class="btn btn-primary btn-sm">Add</button>
		                    	</div>
		                    </div>
	                    </div>
	                    <div class="card-body" id="showStation">
	                        <table class="table table-striped table-sm card-text table table-bordered table-hover table-striped table-td-valign-middle" id="station_table">
							    <thead>
							        <tr>
							            <th>QrCode</th>
							            <th>Station Id</th>
							            <th>Station Name</th>
							            <th>Station Address</th>
							            <th>Description</th>
							            <th>Latitude</th>
							            <th>Longitude</th>
							            <th class="status">Status</th>
							            <th>Action</th>
							        </tr>
							    </thead>
							    <tbody>
							    </tbody>
							</table>
	                    </div>
	                    <div class="card-body" id="AddUpdateStation" style="display: none;">
	                        <form class="form-horizontal" method="post" action="{{url('admin/stations/create')}}" id="stationForm" data-parsley-validate="">
	                        	@csrf
	                        	<input hidden name="station_id" id="station_id">
	                        	<div class="row">
		                        	<div class="col-lg-6">
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Station Name</label>
			                                <div class="col-md-9">
			                                    <input type="text" class="form-control" placeholder="Station Name" name="station_name" id="station_name" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please enter station name">
			                                </div>
			                            </div>
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Station Address</label>
			                                <div class="col-md-9">
			                                    <textarea class="form-control" rows="3" cols="3" placeholder="Address" name="station_addr" id="station_addr" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please enter station address"></textarea>
			                                </div>
			                            </div>
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Description</label>
			                                <div class="col-md-9">
			                                	<textarea class="form-control" rows="3" cols="3" placeholder="Description" name="description" id="description" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please enter description" ></textarea>
			                                </div>
			                            </div>
		                        	</div>
		                        	<div class="col-lg-6">
		                        		 <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Station Id</label>
			                                <div class="col-md-9">
			                                    <input type="text" class="form-control" placeholder="Enter unique station id" name="station_unique_id" id="station_unique_id" data-parsley-trigger="change keyup keydown" required="" data-parsley-required-message="Please enter unique station id">
			                                </div>
			                            </div>
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Latitude</label>
			                                <div class="col-md-9">
			                                    <input type="text" class="form-control" placeholder="Latitude" name="latitude" id="latitude" data-parsley-trigger="change keyup keydown" required="" data-parsley-type="number" data-parsley-required-message="Please enter your latitude">
			                                </div>
			                            </div>
			                            <div class="form-group row">
			                                <label class="col-md-3 form-control-label">Longitude</label>
			                                <div class="col-md-9">
			                                    <input type="text" class="form-control" placeholder="Longitude" name="longitude" id="longitude" data-parsley-trigger="change keyup keydown" required="" data-parsley-type="number" data-parsley-required-message="Please enter your longitude">
			                                </div>
			                            </div>
		                        	</div>		                        	
		                        </div>
	                            <div class="form-group row">
	                                <div class="col-md-9 ml-auto">
	                                    <button type="button" onclick="$('#AddUpdateStation').hide();$('#showStation').show();" class="btn btn-secondary">Cancel</button>
	                                    <button type="submit" class="btn btn-primary">Save changes</button>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	var stationTable;
    $(document).ready(function () {
        stationTable = $('#station_table').DataTable({
            processing: true,
            serverSide: true,
	        order: [],
            ajax: {
                url: BASE_URL + 'stations/get',
				type: 'POST',
			},					
        	columns: [
        		{
        			"render": function (data, type, row, meta) {
		                return "<img src='"+row.qr_code+"'>"
		            },
        		},
                {data: 'station_unique_id', name: 'station_unique_id'},
                {data: 'station_name', name: 'station_name'},
                {data: 'station_addr', name: 'station_addr', searchable: true},
                {data: 'description', name: 'description', searchable: true},
                {data: 'latitude', name: 'latitude', searchable: true},
                {data: 'longitude', name: 'longitude', searchable: true},
                {data: 'status', name: 'status', orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "columnDefs": [{
	            "render": function (data, type, row, meta) {
	                if (type === 'display') {
	                    return (row.status == "1" ? "ACTIVE" : "INACTIVE");
	                }
	                else return "";
	            },
	            "targets": "status"
	        }],
            deferRender: true,
            lengthMenu: [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
            pageLength: 10                   
        });
    });
    function editStation(t){
    	var data = stationTable.row($(t).parents('tr')).data();
    	$("#station_id").val(data.station_id);

    	$("#station_unique_id").val(data.station_unique_id);
    	$("#station_name").val(data.station_name);
    	$("#station_addr").val(data.station_addr);
    	$("#description").val(data.description);
    	$("#latitude").val(data.latitude);
    	$("#longitude").val(data.longitude);
    	$("#stationForm").attr('action',BASE_URL+'stations/update');
    	$("#AddUpdateStation").show();
    	$('#showStation').hide();
    }

    function deleteStation(t){
    	var station_id = stationTable.row($(t).parents('tr')).data().station_id;
		swal({
			title: "Confirmation",
			text: "Are you sure you want to delete this station details?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((isConfirm) => {
			if (isConfirm) {
	            $.ajax({
                	url: BASE_URL+'stations/delete',
                	type: "POST",
                	data: {
                		station_id: station_id
                	},
                	success: function (response) {
                		if(response.status == true){
                    		swal("Done!",response.message,"success");
                		}else{
                			swal("Cancelled",response.message,"error");
                		}
                    	stationTable.ajax.reload();
                	}
	            });
          	}
          	else{
                swal("Cancelled", "", "error");
          	}
		});
    }

    function statusStation(t){
    	var station_id = stationTable.row($(t).parents('tr')).data().station_id;
    	var status = stationTable.row($(t).parents('tr')).data().status;
    	var status1 = "";
    	if(status == "0") {
    		status = 1;
    		status1="Active";
    	}else{
    		status = 0;
    		status1="In-active";
    	}
		swal({
			title: "Confirmation",
			text: "Are you sure you want to change station status to "+status1+"?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((isConfirm) => {
			if (isConfirm) {
	            $.ajax({
                	url: BASE_URL+'stations/update',
                	type: "POST",
                	data: {
                		station_id: station_id,
                		status : status
                	},
                	success: function () {
                    	swal("Done!",status1+" succesfully!","success");
                    	stationTable.ajax.reload();
                	}
	            });
          	}
          	else{
                swal("Cancelled", "", "error");
          	}
		});
    }
</script>
@endsection