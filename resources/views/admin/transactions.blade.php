@extends('layouts.admin')

@section('content')
	<div class="container-fluid px-xl-5">
		<section class="py-5">
			<div class="row">
	            <div class="col-lg-12 mb-5">
	                <div class="card">
	                    <div class="card-header">
	                    	<div class="row">
		                    	<div class="col-lg-6">
		                        	<h3 class="h6 text-uppercase mb-0">User Transaction</h3>
		                    	</div>
		                    </div>
	                    </div>
	                    <div class="card-body" id="showPromotion">
	                        <table class="table table-striped table-sm card-text table table-bordered table-hover table-striped table-td-valign-middle" id="user_transaction_table">
							    <thead>
							        <tr>
							            <th>User Name</th>
							            <th>Mobile No</th>
							            <th>Transaction ID</th>
							            <th>Amount</th>
							            <th>Payment Type</th>
							        </tr>
							    </thead>
							    <tbody>
							    </tbody>
							</table>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	var oTable;
    $(document).ready(function () {
        oTable = $('#user_transaction_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: BASE_URL + 'payment-transactions/get',
				type: 'POST',
			},					
        	columns: [
                /*{
                	render: function(data, meta, row) {
            			return '<a href="mailto:'+row.email+'">'+row.name+'</a><br>'+row.mobile_no;
                	}
                },*/
                {data: 'name', name: 'name', searchable: true},
                {data: 'mobile_no', name: 'mobile_no', searchable: true},
                {data: 'transaction_id', name: 'transaction_id', searchable: true},
                {data: 'amount', name: 'amount', searchable: true},
                {data: 'payment_type', name: 'payment_type'}
            ],
            deferRender: true,
            lengthMenu: [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
            pageLength: 10                   
        });
    });
</script>
@endsection