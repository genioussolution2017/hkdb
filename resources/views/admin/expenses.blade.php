@extends('layouts.admin')

@section('content')
	<div class="row">
		<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 ui-sortable">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<h4 class="panel-title" id="titleExpense">Expenses</h4>
					<div class="panel-heading-btn">
						<button type="button" onclick="$('#AddUpdateExpense').show();$('#showExpensesList').hide();resetForm('data_expenses_addUpdate');$('#data_expenses_addUpdate').attr('action',BASE_URL+'expenses/create');$('#titleExpense').html('Add Expense');" class="btn btn-sm btn-icon btn-circle btn-default"><i class="fa fa-plus"></i></button>&nbsp;&nbsp;
						<button type="button" onclick="$('#AddUpdateExpense').hide();$('#showExpensesList').show();$('#titleExpense').html('Expenses Data');" class="btn btn-sm btn-icon btn-circle btn-success"><i class="fa fa-redo"></i></button>
					</div>
				</div>
				<div class="panel-body" id="showExpensesList" >
					<table id="data_expenses" class="table table-striped table-bordered table-td-valign-middle desktop tablet" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Sr.No</th>
								<th>Image</th>
								<th>Date</th>
								<th>Type</th>
								<th>Value</th>
								<th>Remarks</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="panel-body" id="AddUpdateExpense" style="display: none;">
					<form class="form-horizontal" id="data_expenses_addUpdate" enctype="multipart/form-data" data-parsley-validate="" method="post" action="" name="userfrom" novalidate="">
						@csrf
						<input hidden name="ExpenceDataID" id="ExpenceDataID">
						<div class="row">
							<div class="col-xl-6 col-md-6 col-sm-6 col-xs-12 ui-sortable">

								<div class="form-group row m-b-15">
									<label class="col-md-4 col-sm-4 col-form-label" for="ExpenceOn">Date : </label>
									<div class="col-md-8 col-sm-8">
										<input type="date" class="form-control" name="ExpenceOn" id="ExpenceOn">
									</div>
								</div>

								<div class="form-group row m-b-15">
									<label class="col-md-4 col-sm-4 col-form-label">Select Type <span class="text-danger">*</span> : </label>
									<div class="col-md-8 col-sm-8">
										<select class="form-control" id="ExpenceCommonID" name="ExpenceCommonID" data-parsley-required="true">
											<option value="" >Select Type</option>
											@if (!empty($expenseType))
												@foreach($expenseType as $type)
													<option value="{{$type->MstID}}" >{{$type->MstName}}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>

								<div class="form-group row m-b-15">
									<label class="col-md-4 col-sm-4 col-form-label" for="ExpencesValue">Amount : </label>
									<div class="col-md-8 col-sm-8">
										<input type="number" class="form-control" name="ExpencesValue" id="ExpencesValue">
									</div>
								</div>

								<div class="form-group row m-b-15">
									<label class="col-md-4 col-sm-4 col-form-label" for="ExpenceRemarks">Remarks: </label>
									<div class="col-md-8 col-sm-8">
										<input type="text" class="form-control" name="ExpenceRemarks" id="ExpenceRemarks">
									</div>
								</div>

								<div class="form-group row m-b-15">
									<label class="col-md-4 col-sm-4 col-form-label" for="ExpencesImgPath">Image : </label>
									<div class="col-md-8 col-sm-8">
										<input type="file" class="form-control" name="ExpencesImgPath" id="ExpencesImgPath"  data-parsley-max-file-size="3072">
										<small class="text-danger">Please provide a file smaller than 3 MB:</small>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group row m-b-0">
								<label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
								<div class="col-md-8 col-sm-8">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
		var oTable;
        $(document).ready(function () {
            oTable = $('#data_expenses').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: BASE_URL+'expenses/get',
					type: 'GET',
				},
            	columns: [
            		{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'ExpencesImgPath', name: 'ExpencesImgPath', searchable: true},
                    {data: 'ExpenceOn', name: 'ExpenceOn', searchable: true},
					{data: 'ExpenceCommonID', name: 'ExpenceCommonID', searchable: true},
					{data: 'ExpencesValue', name: 'ExpencesValue', searchable: true},
                    {data: 'ExpenceRemarks', name: 'ExpenceRemarks', searchable: true},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
				],
				order: [],
                deferRender: true,
                lengthMenu: [[10, 15, 25, 50, 100, 1000], [10, 15, 25, 50, 100, 1000]],
                pageLength: 15
            });
        });

        function editExpense(t){
	    	var data = oTable.row($(t).parents('tr')).data();
			$("#ExpenceDataID").val(data.ExpenceDataID);
			$("#ExpenceOn").val(data.ExpenceOn);
			$("#ExpenceCommonID").val(data.ExpenceCommonID);
			$("#ExpencesValue").val(data.ExpencesValue);
			$("#ExpenceRemarks").val(data.ExpenceRemarks);
			$("#ExpencesImgPath").val(data.ExpencesImgPath);

	    	$("#data_expenses_addUpdate").attr('action', BASE_URL+'expenses/update');
	    	$("#AddUpdateExpense").show();
	    	$('#showExpensesList').hide();
	    	$("#titleExpense").html("Edit Expence");
	    }

		function statusExpense(t){
			var id = oTable.row($(t).parents('tr')).data().ExpenceID;

			var status = oTable.row($(t).parents('tr')).data().ExpencesApproveSts;
			status = (status == "Y" ? "N" : "Y");

			swal({
				title: "Confirmation",
				text: "Are you sure you want to update status?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((isConfirm) => {
				if (isConfirm) {
		            $.ajax({
	                	url: BASE_URL+'expenses/update',
						type: "POST",
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
	                	data: {
	                		ExpenceID: id,
	                		ExpencesApproveSts : status
	                	},
	                	success: function () {
	                    	swal("Done!","Expence status updated succesfully!","success");
	                    	oTable.ajax.reload();
	                	}
		            });
	          	}
	          	else{
	                swal("Cancelled", "", "error");
	          	}
			});
	    }

        function deleteExpense(t){
			var id = oTable.row($(t).parents('tr')).data().ExpenceID;
            swal({
                title: "Confirmation",
                text: "Are you sure you want to delete?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            url: BASE_URL+'expenses/delete',
							type: "POST",
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
                            data: {
                                ExpenceID: id
                            },
                            success: function () {
                                swal("Done!","Expence deleted succesfully!","success");
                                oTable.ajax.reload();
                            }
                        });
                    }
                    else{
                        swal("Cancelled", "", "error");
                    }
                });
        }
	</script>
@endsection
