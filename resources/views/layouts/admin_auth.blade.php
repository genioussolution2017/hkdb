<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from seantheme.com/color-admin/admin/html/login_v3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 22 Oct 2019 12:39:29 GMT -->
<head>
	<meta charset="utf-8" />
	<title>HKDB Admin | Login Page</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{ asset('assets/css/default/app.min.css') }}" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
</head>
<body class="pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	@yield('content')
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ asset('assets/js/app.min.js') }}"></script>
	<script src="{{ asset('assets/js/theme/default.min.js') }}"></script>
	<!-- ================== END BASE JS ================== -->

</body>

<!-- Mirrored from seantheme.com/color-admin/admin/html/login_v3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 22 Oct 2019 12:39:30 GMT -->
</html>
