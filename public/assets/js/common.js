$(document).ready(function () {
	/**
	 * Set csrf token header for ajax request in laravel
	 */
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
});
function resetForm(id) {
	$("#"+id).find('input').each(function(){
		if($(this).attr('name') !== '_token') {
			$(this).val('');
		}
	});
	$("#"+id).find('select').each(function(){
		$(this).val('');
	});
	$("#"+id).find('textarea').each(function(){
		$(this).val('');
	});
	$("#"+id).parsley().reset();
}	
/*$('.date').datetimepicker({  
   	format: 'mm-dd-yyyy',
   	autoclose: true
}); 
$(".datetime").datetimepicker({format: 'mm-dd-yyyy hh:ii'});*/