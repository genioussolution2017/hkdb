<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatteryRentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battery_rent_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->unsignedBigInteger('battery_id')->nullable()->default(0);
	        $table->unsignedBigInteger('station_id')->nullable()->default(0);
	        $table->unsignedBigInteger('user_id')->nullable()->default(0);
	        $table->dateTime('start_time')->nullable();
	        $table->dateTime('end_time')->nullable();
	        $table->double('total_amount',10,2)->nullable()->default('0.00');
            $table->timestamps();

	        // Assign foreign key
	        $table->foreign('battery_id')->references('battery_id')->on('batteries');
	        $table->foreign('station_id')->references('station_id')->on('stations');
	        $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battery_rent_histories');
    }
}
