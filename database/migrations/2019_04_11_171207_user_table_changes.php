<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTableChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('is_block')->nullable()->default('0')->comment('0=In active, 1=Active');
        });

        Schema::table('batteries', function (Blueprint $table) {
            $table->string('battery_name', 150)->nullable();
        });

        Schema::table('stations', function (Blueprint $table) {
            $table->string('station_unique_id', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_block');
        });

        Schema::table('batteries', function (Blueprint $table) {
            $table->dropColumn('battery_name');
        });

        Schema::table('stations', function (Blueprint $table) {
            $table->dropColumn('station_unique_id');
        });
    }
}
