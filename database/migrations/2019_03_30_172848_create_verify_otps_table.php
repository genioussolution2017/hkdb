<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifyOtpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_otps', function (Blueprint $table) {
            $table->bigIncrements('otp_id');
	        $table->unsignedBigInteger('user_id')->nullable()->default(0);
	        $table->string('code', 20)->nullable();
            $table->timestamps();

	        // Assign foreign key
	        $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_otps');
    }
}
