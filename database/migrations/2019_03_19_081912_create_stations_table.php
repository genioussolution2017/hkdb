<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations', function (Blueprint $table) {
            $table->bigIncrements('station_id');
            $table->string('station_name', 200)->nullable();
            $table->string('station_addr', 200)->nullable();
            $table->text('description')->nullable();
            $table->double('latitude', 10,4)->nullable()->default('0.00');
            $table->double('longitude', 10,4)->nullable()->default('0.00');
            $table->tinyInteger('status')->nullable()->default('1')->comment('0=In Active, 1=Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
}
