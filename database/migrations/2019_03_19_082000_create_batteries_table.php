<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatteriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('batteries', function (Blueprint $table) {
            $table->bigIncrements('battery_id');
            $table->string('unique_id', 150)->unique();
            $table->string('power_info', 255)->nullable();
            $table->text('battery_desc')->nullable();
            $table->unsignedBigInteger('available_at_station')->nullable()->default(0);
	        $table->tinyInteger('status')->nullable()->default('0')->comment('0=In active, 1=Active');
	        $table->timestamps();

	        // Assign foreign key
	        $table->foreign('available_at_station')->references('station_id')->on('stations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batteries');
    }
}
