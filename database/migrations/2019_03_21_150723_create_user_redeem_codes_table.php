<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRedeemCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_redeem_codes', function (Blueprint $table) {
            $table->bigIncrements('redeem_id');
            $table->unsignedBigInteger('user_id')->nullable()->default(0);
            $table->unsignedBigInteger('promotion_id')->nullable()->default(0);
            $table->double('amount', 10, 2);
            $table->dateTime('redeem_date')->nullable();
            $table->timestamps();

            // Assign foreign key
	        $table->foreign('user_id')->references('id')->on('users');
	        $table->foreign('promotion_id')->references('promotion_id')->on('promotions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_redeem_codes');
    }
}
