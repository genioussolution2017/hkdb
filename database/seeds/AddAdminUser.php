<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddAdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $this->command->info('Running Add admin to user table seed');
	    // check if table users is empty
	    if(DB::table('admins')->get()->count() == 0)
	    {
		    $admin_arr = [
			    'first_name' => config('constants.ADMIN.FNAME'),
			    'last_name' => config('constants.ADMIN.LNAME'),
			    'email_address' => config('constants.ADMIN.EMAIL'),
			    'password' => \Illuminate\Support\Facades\Hash::make(config('constants.ADMIN.PASSWORD')),
			    'created_at' => date('Y-m-d H:i:s'),
			    'updated_at' => date('Y-m-d H:i:s'),
		    ];
		    DB::table('admins')->insert($admin_arr);
		    $this->command->info('Add admin to user using db seed successfully.');
	    }
	    else
	    {
		    $this->command->error('Table is not empty');
	    }
	    $this->command->info('Finish');
    }
}
